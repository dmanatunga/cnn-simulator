#include <string>
#include <iostream>
#include <vector>
#include "utilities/BWImage.h"
#include "cnn/cnn.h"
#include "cpu/CNNArrayCPU.h"

using namespace std;

int main(int argc, char* argv[])
{
  int num_args_needed = 4;
  if (argc != num_args_needed)
  {
    // Error on necessary parameters
    cerr << "Invalid number of arguments. Expected " << num_args_needed << " arguments. Received " << argc-1 << " arguments." << endl;
    return -1;
  }

  string geneName = string(argv[1]);
  string testInputFile = string(argv[2]);
  string outputFile = string(argv[3]);

  CNNGene* gene = CNNGeneFactory::createCNNGene(geneName);

  BWImage testIn, correctOutput;
  testIn.dlmread(testInputFile, " ");
  correctOutput.dlmread(outputFile, " ");

  CNNInput* input = gene->getInput(testIn.getImage(), testIn.R(), testIn.C());
  CNNArrayCPU array(testIn.R(), testIn.C(), gene->r());
  vector<long long int> data = array.outputAnalysisRun(gene, input, correctOutput.getImage());

  cout << geneName << "," << testInputFile;
  for (int i = 0; i < data.size(); i++) {
    cout << "," << data[i];
  }
  cout << endl;

  delete gene;
  delete input;

  return 0;
}
