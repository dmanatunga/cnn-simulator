#! /usr/bin/python
import os
import sys
import re
import csv

def parse_results(filenames):
    out_data = {}
    line_regex = re.compile(r'(?P<gene>\S+),\/?(.+\/)*(?P<test>\S+)_(?P<dim>\d+)x\d+\.dlm,(?P<data>.*)')
    for filename in filenames:
        with open(filename, 'r') as res_file:
            for line in res_file:
                match = line_regex.match(line)
                if match:
                    gene = match.group('gene')
                    dim = int(match.group('dim'))
                    test = match.group('test')
                    data = match.group('data')

                    if not gene in out_data:
                        out_data[gene] = {}
                    gene_data = out_data[gene]
                    
                    if not dim in gene_data:
                        gene_data[dim] = [{}, 0]
                    dim_data = gene_data[dim][0]
                    max_conv_time = gene_data[dim][1]

                    test_data = data.split(',')
                    for i in range(0, len(test_data)):
                        test_data[i] = int(test_data[i])
                    
                    dim_data[test] = test_data
                    if max_conv_time < len(test_data):
                        gene_data[dim][1] = len(test_data)

    
    with open('numdiff.csv', 'w') as out_file:
        out_writer = csv.writer(out_file)
        for gene, gene_data in out_data.iteritems():
            dims = gene_data.keys()
            dims.sort()
            for dim in dims:
                dim_data = gene_data[dim][0]
                max_conv_time = gene_data[dim][1]
                out_writer.writerow(['%s-%d' % (gene, dim)] + range(0, max_conv_time))
                tests = dim_data.keys()
                tests.sort()
                for test in tests:
                    test_data = dim_data[test]
                    out_writer.writerow([test] + test_data)
                out_writer.writerow([])
            out_writer.writerow([])
    
    with open('perdiff.csv', 'w') as out_file:
        out_writer = csv.writer(out_file)
        for gene, gene_data in out_data.iteritems():
            dims = gene_data.keys()
            dims.sort()
            for dim in dims:
                dim_data = gene_data[dim][0]
                max_conv_time = gene_data[dim][1]
                out_writer.writerow(['%s-%d' % (gene, dim)] + range(0, max_conv_time))
                tests = dim_data.keys()
                tests.sort()
                for test in tests:
                    test_data = dim_data[test]
                    data = [float(x) / float(dim ** 2) for x in test_data]
                    out_writer.writerow([test] + data)
                out_writer.writerow([])
            out_writer.writerow([])


if __name__ == "__main__":
    if len(sys.argv) <= 1:
        print('Invalid Number of Arguments.')
    else:
        parse_results(sys.argv[1:])
