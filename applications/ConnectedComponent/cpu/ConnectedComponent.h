#ifndef CONNECTED_COMPONENT_H
#define CONNECTED_COMPONENT_H

#include "utilities/BWImage.h"


void ConnectedComponent(BWImage* in, int* num_comp_list);

#endif // CONNECTED_COMPONENT_H
