#! /usr/bin/python
import os
import sys
import shutil
import re



SIM_TEST_DIR = '../../../test_files'

TESTS = ['lowerA', 'capitalA', 'vertLine', 'circle', 'rect', 'filledSquare', 'zero', 'seven', 'eight', 'nine']
DIMS = [1024, 2048]
BIN = 'connected-component'

RESULTS_DIR = 'results'
OVERALL_FILE = 'overall.csv'
STATS_FILE = 'stats.csv'

def process_file(filename):
    total_time = re.compile(r'Total Time \(ms\): (?P<time_ms>((\d+\.\d+(e-?\d+)?)|(\d+)))')
    with open(filename, 'r') as results_file:
        data = {'gpu_times': [], 'total_times': []}
        for line in results_file:
            result = total_time.match(line)
            if result:
                data['total_times'].append(float(result.group('time_ms')))
        return data

def write_overall_data(data, results_dir):
    output_filename = os.path.join(results_dir, OVERALL_FILE)
    import csv
    with open(output_filename, 'w') as output_file:
        output_writer = csv.writer(output_file)

        for dim in DIMS:
            output_writer.writerow(['%s Total Times' % (dim)])
            for test in TESTS:
                row = [test]
                row.extend(data[dim][test]['total_times'])
                output_writer.writerow(row)
            output_writer.writerow([])
        output_writer.writerow([])

    output_filename = os.path.join(results_dir, STATS_FILE)
    import numpy
    with open(output_filename, 'w') as output_file:
        output_writer = csv.writer(output_file)
        for dim in DIMS:
            header = ['%s %d Total Times' % (BIN, dim)]
            header.extend(['Avg. (drop min-max)', 'Avg', 'Min', 'Max'])
            output_writer.writerow(header)
            for test in TESTS:
                row = [test]
                times = data[dim][test]['total_times']
                min_time = min(times)
                max_time = max(times)
                sum_time = sum(times)
                avg_time = numpy.mean(times)
                n = len(times)
                if n <= 2:
                    avg_drop = 'N/A'
                else:
                    avg_drop = (sum_time - max_time - min_time) / (len(times) - 2)
                
                row.extend([avg_drop, avg_time, min_time, max_time])
                output_writer.writerow(row)
            output_writer.writerow([])
        output_writer.writerow([])



def run_tests():
    results_dir = os.path.join(os.getcwd(), RESULTS_DIR)
    if os.path.exists(results_dir):
        print('Erasing results directory.')
        shutil.rmtree(results_dir)

    os.makedirs(results_dir)

    num_tests = len(TESTS) * len(DIMS)
    test_id = 0
    data = {}
    for dim in DIMS:
        dim_data = {}
        data[dim] = dim_data
        for test in TESTS:
            test_id = test_id + 1
            test_name = '%s_%dx%d' % (test, dim, dim)
            print('Running %s (%d/%d)...' % (test_name, test_id, num_tests))
            test_file = '%s/%s_%dx%d.dlm' % (SIM_TEST_DIR, test, dim, dim)
            output_file = os.path.join(results_dir, '%s.out' % test_name)
            os.system('./%s %s > %s' % (BIN, test_file, output_file))
            test_data = process_file(output_file)
            dim_data[test] = test_data

    print('All tests completed.')
    write_overall_data(data, results_dir)

if __name__ == "__main__":
    run_tests()
