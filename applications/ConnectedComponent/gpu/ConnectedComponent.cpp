#include <string>
#include <iostream>

#include "timing/timer.hpp"

#include "ConnectedComponent.h"

using namespace std;

#define NUM_TRIALS (10)
#define BLOCKSIZE (32)

int main(int argc, char* argv[])
{
	int num_args_needed = 2;
	if (argc != num_args_needed)
	{
			// Error on necessary parameters
		cerr << "Invalid number of arguments. Expected " << num_args_needed << " arguments. Received " << argc << " arguments." << endl;
		return -1;
	}

	string testInputFile = string(argv[1]);
	BWImage testIn;
	testIn.dlmread(testInputFile, " ");

	int* numberConnectedComponent = new int[testIn.R()];
	ConnectedComponentSetup(&testIn, numberConnectedComponent);

	for (int n = 0; n < NUM_TRIALS; ++n)
	{
		timer cnn_timer;
		ConnectedComponent();
		double time_passed = cnn_timer.get_ms();
		cout << "Total Time (ms): " << time_passed << endl;
	}


	ConnectedComponentClose();
	delete[] numberConnectedComponent;
}
