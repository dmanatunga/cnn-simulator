#include <iostream>

#include "ConnectedComponent.h"
#include "cuda_utils.h"

#define GRID_BLOCK_OFFSET_X (32)
#define BLOCKSIZE_Y (256)

#define WHITE (-1)
#define BLACK (1)

	/*
	 * Counts the number of connected black pixels in each row in parallel.
	 * Assumes -1 is white and 1 is black.
	 * Breaks the picture into a grid and each thread in a block loops over a little section of a row.
	 * Then, does a binomial tree approach to parallelize the final sum for each row. Each block
	 * completes the algorithm for a single row.
	 */
__global__ void ConnectedComponentGPU(const size_t rows, const size_t columns,
			float* imageIn, int* numberConnectedComponent,
			int* numberSectionConnectedComponent, int prefixSumArrayPretendLength)
{
	int index_x = blockIdx.x * ((columns - 1) / gridDim.x + 1);
	int index_y = blockIdx.y * blockDim.y + threadIdx.y;

	if (index_y < rows)
	{
		int numberComponentsInBlock = 0;
		float previousValue = (blockIdx.x == 0) ? WHITE : imageIn[index_y * rows + index_x - 1];

		int x_stop = (blockIdx.x + 1) * ((columns - 1) / gridDim.x + 1);

		for (; index_x < x_stop && index_x < columns; ++index_x)
		{
			float currentValue = imageIn[index_y * rows + index_x];
			if (previousValue == WHITE && currentValue == BLACK)
				++numberComponentsInBlock;

			previousValue = currentValue;
		}

		numberSectionConnectedComponent[index_y * gridDim.x + blockIdx.x] = numberComponentsInBlock;

		/* Naive sum approach.
		if (blockIdx.x == 0)
		{
			int localNumberConnectedComponent = 0;
			for (int n = 0; n < gridDim.x; ++n)
				localNumberConnectedComponent += numberSectionConnectedComponent[index_y * gridDim.x + n];
			numberConnectedComponent[index_y] = localNumberConnectedComponent;
		}*/
	}

		// Up sweep section of prefix sum, then only takes final index value.
		// Works for any size array.
		// Pads the array to be 2^x >= n in length; the actual array is the end values.
	int blockIDSum = gridDim.y * blockIdx.x + blockIdx.y;
	if (blockIDSum < rows)
	{
		int threadID = threadIdx.y;
		int offset = 1;
		for (int iteration = prefixSumArrayPretendLength >> 1; iteration > 0; iteration >>= 1)
		{
			__syncthreads();
			if (threadID < iteration)
			{
				int indexA = offset * (2 * threadID + 1) - 1 - (prefixSumArrayPretendLength - gridDim.x);
				int indexB = offset * (2 * threadID + 2) - 1 - (prefixSumArrayPretendLength - gridDim.x);

				if (indexA >= 0 && indexB >= 0)
					numberSectionConnectedComponent[blockIDSum * gridDim.x + indexA]
						+= numberSectionConnectedComponent[blockIDSum * gridDim.x + indexB];
			}
			offset *= 2;
		}

		if (threadID == 0)
			numberConnectedComponent[blockIDSum] = numberSectionConnectedComponent[blockIDSum * gridDim.x + gridDim.x - 1];
	}
}

static float* h_imageIn;
static int*   h_numberConnectedComponent;
static float* d_imageIn;
static int*   d_numberConnectedComponent;
static int*   d_numberSectionConnectedComponent;
static size_t rows;
static size_t columns;
static dim3   blockSize;
static dim3   gridSize;
static int    prefixSumArrayPretendLength;
void ConnectedComponent()
{
	using namespace std;

	cudaEvent_t start, stop;

	CUDA_CHECK_ERROR (cudaEventCreate (&start));
	CUDA_CHECK_ERROR (cudaEventCreate (&stop));
	float elapsedTime;
	CUDA_CHECK_ERROR (cudaEventRecord (start, 0));
	ConnectedComponentGPU<<<gridSize, blockSize>>>(rows, columns,
			d_imageIn, d_numberConnectedComponent, d_numberSectionConnectedComponent,
			prefixSumArrayPretendLength);
	cudaThreadSynchronize();

        CUDA_CHECK_ERROR (cudaEventRecord (stop, 0));
	CUDA_CHECK_ERROR (cudaEventSynchronize (stop));
	CUDA_CHECK_ERROR (cudaEventElapsedTime (&elapsedTime, start, stop));
	elapsedTime = elapsedTime / 5;

	fprintf (stdout, "GPU Time (ms): %f\n", elapsedTime);

	cudaError_t code;
	if (cudaSuccess != (code = cudaDeviceSynchronize()))
	{
		cerr << "GPU ERROR: " << cudaGetErrorString(code) << " " << __FILE__ << " " << __LINE__ << endl;
		exit(code);
	}
}

void ConnectedComponentSetup(BWImage* in, int* numberConnectedComponent)
{
	h_imageIn = in->getImage();
	h_numberConnectedComponent = numberConnectedComponent;

	rows = in->R();
	columns = in->C();

	blockSize.x = 1;
	blockSize.y = BLOCKSIZE_Y;

	gridSize.y = (rows - 1) / blockSize.y + 1;
	//gridSize.x = (columns - 1) / GRID_BLOCK_OFFSET_X + 1;
	gridSize.x = (rows - 1) / gridSize.y + 1;
	if (gridSize.x > 2 * BLOCKSIZE_Y)
		gridSize.x = 2 * BLOCKSIZE_Y;

	prefixSumArrayPretendLength = 1;
	while (prefixSumArrayPretendLength < gridSize.x)
		prefixSumArrayPretendLength *= 2;

	if (rows > gridSize.x * gridSize.y)
	{
		cerr << "Not enough blocks to calculate all sums of each row " << endl;
		cerr << "rows = " << rows << ", gridSize.x * gridSize.y = " << (gridSize.x * gridSize.y)
			<< " (x = " << gridSize.x << ", y = " << gridSize.y << ")" << endl;
		exit(0);
	}

	cudaMalloc((void**)&d_imageIn, sizeof(float) * rows * columns);
	cudaMalloc((void**)&d_numberConnectedComponent, sizeof(int) * rows);
	cudaMalloc((void**)&d_numberSectionConnectedComponent, sizeof(int) * rows * gridSize.x);

	cudaMemcpy(d_imageIn, h_imageIn, sizeof(float) * rows * columns, cudaMemcpyHostToDevice);
	cudaMemcpy(d_numberConnectedComponent, h_numberConnectedComponent,
	           sizeof(float) * rows, cudaMemcpyHostToDevice);
}

void ConnectedComponentClose()
{
	cudaMemcpy(h_numberConnectedComponent, d_numberConnectedComponent,
	           sizeof(float) * rows, cudaMemcpyDeviceToHost);

	cudaFree(d_imageIn);
	cudaFree(d_numberConnectedComponent);
	cudaFree(d_numberSectionConnectedComponent);
}
