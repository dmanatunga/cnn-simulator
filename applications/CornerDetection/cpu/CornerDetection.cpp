#include <string>
#include <iostream>

#include "timing/timer.hpp"

#include "CornerDetection.h"

using namespace std;

#define NUM_TRIALS 10

void CornerDetection(BWImage* in, BWImage* out) 
{
  float* img = in->getImage();
  float* img2 = out->getImage();
  size_t R = in->R();
  size_t C = in->C();

  for (int i = 0; i < R; i++)  {
    int numWhite = 0;
    int minI = (i == 0) ? 0 : i - 1;
    int maxI = (i == (R - 1)) ? R - 1 :  i + 1;
    int array[9] = {-1};

    for (int j = 0; j < C; j++) {
      if (img[i * R + j] == 1) {
        int minJ = (j == 0) ? 0 : j - 1;
        int maxJ = (j == (C - 1)) ? C - 1 : j + 1;

        int numWhite = 9 - (maxI - minI) * (maxJ - minJ);
        for (int dx = minI; dx < maxI; dx++) {
          for (int dy = minJ; dy < maxJ; dy++) {  
            if (img[dx * R + dy] == -1) 
              numWhite++;
          } 
        }

        if (numWhite >= 5)
          img2[i * R + j] = 1;
        else
          img2[i * R + j] = -1;
      } else {
        img2[i * R + j] = -1;
      }
    }
  }
}


int main(int argc, char* argv[])
{
  int num_args_needed = 2;
  if (argc != num_args_needed)
  {
    // Error on necessary parameters
    cerr << "Invalid number of arguments. Expected " << num_args_needed << " arguments. Received " << argc << " arguments." << endl;
    return -1;
  }

  string testInputFile = string(argv[1]);
  BWImage testIn;
  testIn.dlmread(testInputFile, " ");
  BWImage testOut(testIn.R(), testIn.C());

  for (int i = 0; i < NUM_TRIALS; i++) {
    timer cnn_timer;
    CornerDetection(&testIn, &testOut);
    double time_passed = cnn_timer.get_ms(); 
    cout << "Total Time (ms): " << time_passed << endl;
  }
}
