#ifndef CORNER_DETECTION_H
#define CORNER_DETECTION_H

#include "utilities/BWImage.h"

void CornerDetection();
void CornerDetectionSetup(BWImage* in, BWImage* out);
void CornerDetectionClose();

#endif
