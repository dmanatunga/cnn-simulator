#include <iostream>

#include "CornerDetection.h"
#include "cuda_utils.h"

#define BLOCKSIZE_X (16)
#define BLOCKSIZE_Y (16)

	/*
	 * Uses 2D convolution without padding of a single value (mimics middle value for out of bounds areas)
	 * Laplacian Corner Detection Approximation.
	 */
__global__ void CornerDetectionGPU(const size_t rows, const size_t columns, float* imageIn, float* imageOut)
{
	int index_x = blockIdx.x * blockDim.x + threadIdx.x;
	int index_y = blockIdx.y * blockDim.y + threadIdx.y;

	if (index_x < columns && index_y < rows)
	{
		int index_mid   = index_y * rows + index_x;
		
		int left  = (index_x == 0)           ? 0 : -1;
		int right = (index_x == columns - 1) ? 0 :  1;
		
		int up   = (index_y == 0)        ? 0 : -rows;
		int down = (index_y == rows - 1) ? 0 :  rows;
		
		int index_left  = index_mid + left;  //index_y * rows + index_x - 1;
		int index_right = index_mid + right; //index_y * rows + index_x + 1;
		int index_up    = index_mid + up;    //(index_y - 1) * rows + index_x;
		int index_down  = index_mid + down;  //(index_y + 1) * rows + index_x;
	
		int index_LU = index_mid + left + up;
		int index_RU = index_mid + right + up;
		int index_LD = index_mid + left + down;
		int index_RD = index_mid + right + down;

			// Laplacian Filter Corner Detection
		imageOut[index_mid] =  (4 * imageIn[index_mid]) - 5 -imageIn[index_left]
			-imageIn[index_right] - imageIn[index_up] - imageIn[index_down]
		                    - imageIn[index_LU]   - imageIn[index_RU]
				    -imageIn[index_LD] - imageIn[index_RD];

		if (imageOut[index_mid] > 1)
			imageOut[index_mid] =  1;
		else if (imageOut[index_mid] < -1)
			imageOut[index_mid] = -1;
	}
}

static float* h_imageIn;
static float* h_imageOut;
static float* d_imageIn;
static float* d_imageOut;
static size_t rows;
static size_t columns;
static dim3 blockSize;
static dim3 gridSize;
void CornerDetection()
{
	using namespace std;
	cudaEvent_t start, stop;

	CUDA_CHECK_ERROR (cudaEventCreate (&start));
	CUDA_CHECK_ERROR (cudaEventCreate (&stop));
	float elapsedTime;
	CUDA_CHECK_ERROR (cudaEventRecord (start, 0));

	
	CornerDetectionGPU<<<gridSize, blockSize>>>(rows, columns, d_imageIn, d_imageOut);
	cudaThreadSynchronize();

        CUDA_CHECK_ERROR (cudaEventRecord (stop, 0));
	CUDA_CHECK_ERROR (cudaEventSynchronize (stop));
	CUDA_CHECK_ERROR (cudaEventElapsedTime (&elapsedTime, start, stop));
	elapsedTime = elapsedTime / 5;

	fprintf (stdout, "GPU Time (ms): %f\n", elapsedTime);

	cudaError_t code;
	if (cudaSuccess != (code = cudaDeviceSynchronize()))
	{
		cerr << "GPU ERROR: " << cudaGetErrorString(code) << " " << __FILE__ << " " << __LINE__ << endl;
		//fprintf(stderr, "GPU ERROR: %s %s %d\n", cudaGetErrorString(code), __FILE__, __LINE__);
		exit(code);
	}
}

void CornerDetectionSetup(BWImage* in, BWImage* out)
{
	h_imageIn = in->getImage();
	h_imageOut = out->getImage();

	rows = in->R();
	columns = in->C();

	blockSize.x = BLOCKSIZE_X;
	blockSize.y = BLOCKSIZE_Y;

	gridSize.x = (columns - 1) / blockSize.x + 1;
	gridSize.y = (rows - 1) / blockSize.y + 1;

	cudaMalloc((void**)&d_imageIn, sizeof(float) * rows * columns);
	cudaMalloc((void**)&d_imageOut, sizeof(float) * rows * columns);
	cudaMemcpy(d_imageIn, h_imageIn, sizeof(float) * rows * columns, cudaMemcpyHostToDevice);
	cudaMemcpy(d_imageOut, h_imageOut, sizeof(float) * rows * columns, cudaMemcpyHostToDevice);
}

void CornerDetectionClose()
{
	cudaMemcpy(h_imageOut, d_imageOut, sizeof(float) * rows * columns, cudaMemcpyDeviceToHost);

	cudaFree(d_imageIn);
	cudaFree(d_imageOut);
}
