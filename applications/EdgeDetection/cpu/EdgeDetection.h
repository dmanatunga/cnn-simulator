#ifndef EDGE_DETECTION_H
#define EDGE_DETECTION_H

#include "utilities/BWImage.h"


void EdgeDetection(BWImage* in, BWImage* out);

#endif // EDGE_DETECTION_H
