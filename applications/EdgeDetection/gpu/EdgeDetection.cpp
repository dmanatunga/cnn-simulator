#include <string>
#include <iostream>

#include "timing/timer.hpp"

#include "EdgeDetection.h"

using namespace std;

#define NUM_TRIALS (10)
#define BLOCKSIZE (32)

//__global__ void EdgeDetectionGPU(const size_t rows, const size_t columns, float* imageIn, float* imageOut);

/*void EdgeDetection(BWImage* in, BWImage* out)
{
	float* h_imageIn = in->getImage();
	float* h_imageOut = out->getImage();

	const size_t rows = in->R();
	const size_t columns = in->C();

	dim3 blockSize;
	blockSize.x = BLOCKSIZE;
	blockSize.y = BLOCKSIZE;

	dim3 gridSize;
	gridSize.x = (columns - 1) / blockSize.x + 1;
	gridSize.y = (rows - 1) / blockSize.y + 1;

	float* d_imageIn;
	float* d_imageOut;
	cudaMalloc((void**)&d_imageIn, sizeof(float) * rows * columns);
	cudaMalloc((void**)&d_imageOut, sizeof(float) * rows * columns);
	cudaMemcpy(d_imageIn, h_imageIn, sizeof(float) * rows * columns, cudaMemcpyHostToDevice);
	cudaMemcpy(d_imageOut, h_imageOut, sizeof(float) * rows * columns, cudaMemcpyHostToDevice);

	EdgeDetectionGPU<<<gridSize, blockSize>>>(rows, columns, d_imageIn, d_imageOut);

	cudaError_t code;
	if (cudaSucess != (code = cudaDeviceSynchronize()))
	{
		fprintf(stderr, "GPU ERROR: %s %s %d\n", cudaGetErrorString(code), __FILE__, __LINE__);
		exit(code);
	}

	cudaMemcpy(h_imageOut, d_imageOut, sizeof(float) * rows * columns, cudaMemcpyDeviceToHost);

	cudaFree(d_imageIn);
	cudaFree(d_imageOut);
}*/

int main(int argc, char* argv[])
{
	int num_args_needed = 2;
	if (argc != num_args_needed)
	{
			// Error on necessary parameters
		cerr << "Invalid number of arguments. Expected " << num_args_needed << " arguments. Received " << argc << " arguments." << endl;
		return -1;
	}

	string testInputFile = string(argv[1]);
	BWImage testIn;
	testIn.dlmread(testInputFile, " ");
	BWImage testOut(testIn.R(), testIn.C());

	EdgeDetectionSetup(&testIn, &testOut);

	for (int n = 0; n < NUM_TRIALS; ++n)
	{
		timer cnn_timer;
		EdgeDetection();
    double time_passed = cnn_timer.get_ms(); 
    cout << "Total Time (ms): " << time_passed << endl;
	}


	EdgeDetectionClose();
}
