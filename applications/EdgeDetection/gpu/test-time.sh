
declare -a dims=(64 128 256 512 1024 2048 4096)
test=spiral
test_dir=../../../test_files

for dim in ${dims[@]} ; do
  echo $dim
  ./edge-detection ${test_dir}/${test}_${dim}x${dim}.dlm
  echo ""
done
