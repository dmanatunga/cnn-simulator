# CNN CPU makefile
CC := g++
BUILDDIR := build

PROJECT_ROOT_DIR = ../../..

UTILITIES_DIR := $(PROJECT_ROOT_DIR)/utilities
UTILITIES_SOURCES := $(wildcard $(UTILITIES_DIR)/BWImage.cpp)
UTILITIES_OBJECTS = $(patsubst $(UTILITIES_DIR)/%, $(BUILDDIR)/%, $(UTILITIES_SOURCES:.cpp=.o))

TIMING_DIR := $(PROJECT_ROOT_DIR)/timing
TIMING_SOURCES := $(wildcard $(TIMING_DIR)/*.cpp)
TIMING_OBJECTS = $(patsubst $(TIMING_DIR)/%, $(BUILDDIR)/%, $(TIMING_SOURCES:.cpp=.o))

APP_DIR := .
APP_SOURCES := $(wildcard $(APP_DIR)/*.cpp)
APP_OBJECTS = $(patsubst $(APP_DIR)/%, $(BUILDDIR)/%, $(APP_SOURCES:.cpp=.o))

CUDA_DIR := .
CUDA_SOURCES := $(wildcard $(CUDA_DIR)/*.cu)
CUDA_OBJECTS = $(patsubst $(CUDA_DIR)/%, $(BUILDDIR)/%, $(CUDA_SOURCES:.cu=.o))


SOURCES := $(UTILITIES_SOURCES) $(APP_SOURCES) $(TIMING_SOURCES) #$(TEST_SOURCES)
OBJECTS := $(UTILITIES_OBJECTS) $(APP_OBJECTS) $(TIMING_OBJECTS) #$(TEST_OBJECTS)

DEPS := $(OBJECTS:.o=.deps)

TARGETS := shadow-creator

override CXXFLAGS += $(patsubst %, -I%, $(PROJECT_ROOT_DIR) .) 
override CXXFLAGS += -O3 -std=gnu++0x
override LDFLAGS += -static-libgcc

override CXXFLAGS_CUDA += $(patsubst %, -I%, $(PROJECT_ROOT_DIR) .)
override CXXFLAGS_CUDA += -O3

define cc-command
@mkdir -p $(BUILDDIR)
@echo "Compile $<"; 
$(CXX) $(CXXFLAGS) -MD -MF $(@:.o=.deps) -c -o $@ $<
endef

.PHONY: clean all

CUDA_PATH ?= "/usr/local/cuda-6.0"

OSUPPER = $(shell uname -s 2>/dev/null | tr "[:lower:]" "[:upper:]")
OSLOWER = $(shell uname -s 2>/dev/null | tr "[:upper:]" "[:lower:]")

OS_SIZE = $(shell uname -m | sed -e "s/i.86/32/" -e "s/x86_64/64/" -e "s/armv7l/32/")
OS_ARCH = $(shell uname -m | sed -e "s/i386/i686/")

DARWIN = $(strip $(findstring DARWIN, $(OSUPPER)))
ifneq ($(DARWIN),)
	XCODE_GE_5 = $(shell expr `xcodebuild -version | grep -i xcode | awk '{print $$2}' | cut -d'.' -f1` \>= 5)
endif

# Take command line flags that override any of these settings
ifeq ($(i386),1)
	OS_SIZE = 32
	OS_ARCH = i686
endif
ifeq ($(x86_64),1)
	OS_SIZE = 64
	OS_ARCH = x86_64
endif
ifeq ($(ARMv7),1)
	OS_SIZE = 32
	OS_ARCH = armv7l
endif

# Common binaries
ifneq ($(DARWIN),)
ifeq ($(XCODE_GE_5),1)
  GCC ?= clang
else
  GCC ?= g++
endif
else
  GCC ?= g++
endif
NVCC := $(CUDA_PATH)/bin/nvcc -ccbin $(GCC)

# internal flags
NVCCFLAGS   := -m${OS_SIZE}
CCFLAGS     :=
LDFLAGS     :=

# Extra user flags
EXTRA_NVCCFLAGS   ?=
EXTRA_LDFLAGS     ?=
EXTRA_CCFLAGS     ?=

# OS-specific build flags
ifneq ($(DARWIN),)
  LDFLAGS += -rpath $(CUDA_PATH)/lib
  CCFLAGS += -arch $(OS_ARCH)
else
  ifeq ($(OS_ARCH),armv7l)
    ifeq ($(abi),gnueabi)
      CCFLAGS += -mfloat-abi=softfp
    else
      # default to gnueabihf
      override abi := gnueabihf
      LDFLAGS += --dynamic-linker=/lib/ld-linux-armhf.so.3
      CCFLAGS += -mfloat-abi=hard
    endif
  endif
endif

ifeq ($(ARMv7),1)
NVCCFLAGS += -target-cpu-arch ARM
ifneq ($(TARGET_FS),)
CCFLAGS += --sysroot=$(TARGET_FS)
LDFLAGS += --sysroot=$(TARGET_FS)
LDFLAGS += -rpath-link=$(TARGET_FS)/lib
LDFLAGS += -rpath-link=$(TARGET_FS)/usr/lib
LDFLAGS += -rpath-link=$(TARGET_FS)/usr/lib/arm-linux-$(abi)
endif
endif

# Debug build flags
ifeq ($(dbg),1)
      NVCCFLAGS += -g -G
      TARGET := debug
else
      TARGET := release
endif

ALL_CCFLAGS :=
ALL_CCFLAGS += $(NVCCFLAGS)
ALL_CCFLAGS += $(EXTRA_NVCCFLAGS)
ALL_CCFLAGS += $(addprefix -Xcompiler ,$(CCFLAGS))
ALL_CCFLAGS += $(addprefix -Xcompiler ,$(EXTRA_CCFLAGS))

ALL_LDFLAGS :=
ALL_LDFLAGS += $(ALL_CCFLAGS)
ALL_LDFLAGS += $(addprefix -Xlinker ,$(LDFLAGS))
ALL_LDFLAGS += $(addprefix -Xlinker ,$(EXTRA_LDFLAGS))

# Common includes and paths for CUDA
INCLUDES  := -I../../common/inc $(patsubst %, -I%, $(PROJECT_ROOT_DIR) .)
LIBRARIES :=

################################################################################

# CUDA code generation flags
ifneq ($(OS_ARCH),armv7l)
GENCODE_SM10    := -gencode arch=compute_10,code=sm_10
endif
GENCODE_SM20    := -gencode arch=compute_20,code=sm_20
GENCODE_SM30    := -gencode arch=compute_30,code=sm_30
GENCODE_SM32    := -gencode arch=compute_32,code=sm_32
GENCODE_SM35    := -gencode arch=compute_35,code=sm_35
GENCODE_SM50    := -gencode arch=compute_50,code=sm_50
GENCODE_SMXX    := -gencode arch=compute_50,code=compute_50
GENCODE_FLAGS   ?= $(GENCODE_SM10) $(GENCODE_SM20) $(GENCODE_SM30) $(GENCODE_SM32) $(GENCODE_SM35) $(GENCODE_SM50) $(GENCODE_SMXX)

################################################################################

# Target rules
all: shadow-creator

shadow-creator: $(UTILITIES_OBJECTS) $(TIMING_OBJECTS) $(APP_OBJECTS) $(CUDA_OBJECTS)
	$(CXX) $(LDFLAGS) -o $@ $^ -lrt -ldl -L/usr/local/cuda-6.0/lib -lcudart

$(APP_OBJECTS) : $(BUILDDIR)/%.o : $(APP_DIR)/%.cpp
	$(cc-command)

$(UTILITIES_OBJECTS) : $(BUILDDIR)/%.o : $(UTILITIES_DIR)/%.cpp
	$(cc-command)

#$(TEST_OBJECTS) : $(BUILDDIR)/%.o : $(TEST_DIR)/%.cpp
#	$(cc-command)

$(TIMING_OBJECTS) : $(BUILDDIR)/%.o : $(TIMING_DIR)/%.cpp
	$(cc-command) -lrt -ldl

$(CUDA_OBJECTS) : $(BUILDDIR)/%.o : $(CUDA_DIR)/%.cu
	$(NVCC) $(INCLUDES) $(ALL_CCFLAGS) $(GENCODE_FLAGS) -o $@ -c $<

clean:
	@echo " Cleaning..."; $(RM) -r -f $(BUILDDIR) $(TARGETS)

-include $(DEPS)
