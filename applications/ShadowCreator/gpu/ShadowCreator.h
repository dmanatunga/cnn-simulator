#ifndef SHADOW_CREATOR_H
#define SHADOW_CREATOR_H

#include "utilities/BWImage.h"

void ShadowCreator();
void ShadowCreatorSetup(BWImage* in, BWImage* out);
void ShadowCreatorClose();

#endif
