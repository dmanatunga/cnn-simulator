#include "CNNArrayCPU.h"
#include <cmath>
#include <string.h>
#include <assert.h>
#include <iostream>
#include <vector>

CNNArrayCPU::CNNArrayCPU(size_t M, size_t N, size_t r)
{
	_M = M;
	_N = N;
	_r = r;

	_R = M + 2 * r;
	_C = N + 2 * r;

	_state = new float[_R * _C];
	_output = new float[_R * _C];
}

CNNArrayCPU::~CNNArrayCPU() 
{
	delete[] _state;
	delete[] _output;
}

void CNNArrayCPU::run(CNNGene* gene, CNNInput* input, unsigned int N, float dt)
{
	assert(gene->r() == _r);
	assert(input->R() == _R);
	assert(input->C() == _C);

	_gene = gene;
	_input = input;
	// Copy the initial state over
	memcpy(_state, input->initialState(), sizeof(float) * _R * _C);
	this->computeInitialOutput();
	for (unsigned int i = 1; i <= N; i++) {
		this->computeNextState(dt);
		this->computeOutput();
	}
}

unsigned int CNNArrayCPU::convergenceRun(CNNGene* gene, CNNInput* input, float dt)
{
	assert(gene->r() == _r);
	assert(input->R() == _R);
	assert(input->C() == _C);

	_gene = gene;
	_input = input;
	// Copy the initial state over
	memcpy(_state, input->initialState(), sizeof(float) * _R * _C);
	this->computeInitialOutput();
	bool change = true;
	unsigned int N = 0;
	while (change) {
		change = this->computeNextState(dt);
		this->computeOutput();
		N++;
	}
	return N;
}

bool CNNArrayCPU::computeNextState(const float dt)
{
	const float z = _gene->z();
	const float* A  = _gene->A();
	const float* B = _gene->B();
	const float* u = _input->u();

	bool change = false;
	for (size_t i = 0; i < _M; ++i) {
		for (size_t j = 0; j < _N; ++j) {
			float stateVal = _state[(i + _r) * _C + (j + _r)];

			float stateDeriv = -stateVal + z;
			for (size_t k = 0; k < (2 * _r + 1); k++) {
				for (size_t l = 0; l < (2 * _r + 1); l++) {
					stateDeriv += A[k * (2 * _r + 1) + l] * _output[(i + k) * _C + (j + l)];
					stateDeriv += B[k * (2 * _r + 1) + l] * u[(i + k) * _C + (j + l)];
				}
			}
			float nextState = stateVal + stateDeriv * dt;
			if (abs(nextState - stateVal) >= CHANGE_THRESHOLD) {
				change = true;
			}

			_state[(i + _r) * _C + (j + _r)] = nextState;
		}
	}
	return change;
}

void CNNArrayCPU::run(CNNGene* gene, CNNInput* input, unsigned int N)
{
	assert(gene->r() == _r);
	assert(input->R() == _R);
	assert(input->C() == _C);

	_gene = gene;
	_input = input;

	// Copy the initial state over
	memcpy(_state, input->initialState(), sizeof(float) * _R * _C);
	this->computeInitialOutput();
	for (unsigned int i = 1; i <= N; i++) {
		this->computeNextState();
		this->computeOutput();
	}
}

unsigned int CNNArrayCPU::convergenceRun(CNNGene* gene, CNNInput* input)
{
	assert(gene->r() == _r);
	assert(input->R() == _R);
	assert(input->C() == _C);

	_gene = gene;
	_input = input;

	// Copy the initial state over
	memcpy(_state, input->initialState(), sizeof(float) * _R * _C);
	this->computeInitialOutput();
	bool change = true;
	unsigned int N = 0;
	while (change) {
		change = this->computeNextState();
		this->computeOutput();
		N++;
	}
	return N;
}

std::vector<long long int> CNNArrayCPU::outputAnalysisRun(CNNGene* gene, CNNInput* input, float* idealOutput)
{
	assert(gene->r() == _r);
	assert(input->R() == _R);
	assert(input->C() == _C);

	_gene = gene;
	_input = input;
	std::vector<long long int> data;
	// Copy the initial state over
	memcpy(_state, input->initialState(), sizeof(float) * _R * _C);
	this->computeInitialOutput();

	long long int numDiff = compareOutput(idealOutput);
	data.push_back(numDiff);

	bool change = true;
	unsigned int N = 0;
	while (change) {
		change = this->computeNextState();
		this->computeOutput();
		numDiff = compareOutput(idealOutput);
		data.push_back(numDiff);
		N++;
	}

	return data;
}

bool CNNArrayCPU::computeNextState()
{
	const float z = _gene->z();
	const float* A  = _gene->A();
	const float* B = _gene->B();
	const float* u = _input->u();

	bool change = false;
	for (size_t i = 0; i < _M; ++i) {
		for (size_t j = 0; j < _N; ++j) {
			float delta = z;
			for (size_t k = 0; k < (2 * _r + 1); k++) {
				for (size_t l = 0; l < (2 * _r + 1); l++) {
					delta += A[k * (2 * _r + 1) + l] * _output[(i + k) * _C + (j + l)];
					delta += B[k * (2 * _r + 1) + l] * u[(i + k) * _C + (j + l)];
				}
			}

			if (abs(_state[(i + _r) * _C + (j + _r)] - delta) >= CHANGE_THRESHOLD) {
				change = true;
			}

			_state[(i + _r) * _C + (j + _r)] = delta;
		}
	}
	return change;
}

void CNNArrayCPU::computeInitialOutput()
{
	for (size_t i = 0; i < _R; ++i) {
		for (size_t j = 0; j < _C; ++j) {
			float stateVal = _state[i * _C + j];
			_output[i * _C + j] = (abs(stateVal + 1) - abs(stateVal - 1)) / 2.0f;
		}
	}
}

void CNNArrayCPU::computeOutput()
{
	for (size_t i = 0; i < _M; ++i) {
		for (size_t j = 0; j < _N; ++j) {
			float stateVal = _state[(i + _r) * _C + (j + _r)];
			_output[(i + _r) * _C + (j + _r)] = (abs(stateVal + 1) - abs(stateVal - 1)) / 2.0f;
		}
	}
}

long long int CNNArrayCPU::compareOutput(float* ideal)
{
	long long int numDiff = 0;
	for (size_t i = 0; i < _M; ++i) {
		for (size_t j = 0; j < _N; ++j) {
			float val1 = _output[(i + _r) * _C + (j + _r)];
			float val2 = ideal[i * _N + j];
			if (abs(val1 - val2) >= COMPARE_THRESHOLD)
			{
				numDiff++;
			}
		}
	}
	return numDiff;
}

void CNNArrayCPU::copyOutput(float* out)
{
	for (size_t i = 0; i < _M; ++i) {
		for (size_t j = 0; j < _N; ++j) {
			out[i* _N + j] = _output[(i + _r) * _C + (j + _r)];
		}
	}
}


////////////////////////////////////////////////////////////////////
// CNNArrayCPU - Increased Memory Usage Version
////////////////////////////////////////////////////////////////////
CNNArrayCPUv2::CNNArrayCPUv2(size_t M, size_t N, size_t r)
{
	_M = M;
	_N = N;
	_r = r;

	_R = M + 2 * r;
	_C = N + 2 * r;

	_state = new float[_R * _C];
	_output = new float[_R * _C];
	_tmp_output = new float[_R * _C];
}

CNNArrayCPUv2::~CNNArrayCPUv2() 
{
	delete[] _state;
	delete[] _output;
	delete[] _tmp_output;
}

void CNNArrayCPUv2::run(CNNGene* gene, CNNInput* input, unsigned int N, float dt)
{
	assert(gene->r() == _r);
	assert(input->R() == _R);
	assert(input->C() == _C);

	_gene = gene;
	_input = input;

	// Copy the initial state over
	memcpy(_state, input->initialState(), sizeof(float) * _R * _C);
	this->computeInitialOutput();
	for (unsigned int i = 1; i <= N; i++) {
		this->computeStateAndOutput(dt);
		float* tmp = _output;
		_output = _tmp_output;
		_tmp_output = tmp;
	}
}

void CNNArrayCPUv2::run(CNNGene* gene, CNNInput* input, unsigned int N)
{
	assert(gene->r() == _r);
	assert(input->R() == _R);
	assert(input->C() == _C);

	_gene = gene;
	_input = input;

	// Copy the initial state over
	memcpy(_output, input->initialState(), sizeof(float) * _R * _C);
	this->computeInitialOutput2();
	for (unsigned int i = 1; i <= N; i++) {
		this->computeStateAndOutput();
		float* tmp = _output;
		_output = _tmp_output;
		_tmp_output = tmp;
	}
}

void CNNArrayCPUv2::computeStateAndOutput(const float dt)
{
	const float z = _gene->z();
	const float* A  = _gene->A();
	const float* B = _gene->B();
	const float* u = _input->u();

	for (size_t i = 0; i < _M; ++i) {
		for (size_t j = 0; j < _N; ++j) {
			float stateVal = _state[(i + _r) * _C + (j + _r)];

			float stateDeriv = -stateVal + z;
			for (size_t k = 0; k < (2 * _r + 1); k++) {
				for (size_t l = 0; l < (2 * _r + 1); l++) {
					stateDeriv += A[k * (2 * _r + 1) + l] * _output[(i + k) * _C + (j + l)];
					stateDeriv += B[k * (2 * _r + 1) + l] * u[(i + k) * _C + (j + l)];
				}
			}

			stateVal += stateDeriv * dt;
			_state[(i + _r) * _C + (j + _r)] = stateVal;
			_tmp_output[(i + _r) * _C + (j + _r)] = (abs(stateVal + 1) - abs(stateVal - 1)) / 2.0f;
		}
	}
}


void CNNArrayCPUv2::computeStateAndOutput()
{
	const float z = _gene->z();
	const float* A  = _gene->A();
	const float* B = _gene->B();
	const float* u = _input->u();

	for (size_t i = 0; i < _M; ++i) {
		for (size_t j = 0; j < _N; ++j) {
			float delta = z;
			for (size_t k = 0; k < (2 * _r + 1); k++) {
				for (size_t l = 0; l < (2 * _r + 1); l++) {
					delta += A[k * (2 * _r + 1) + l] * _output[(i + k) * _C + (j + l)];
					delta += B[k * (2 * _r + 1) + l] * u[(i + k) * _C + (j + l)];
				}
			}

			//_state[(i + _r) * _C + (j + _r)] = delta;
			_tmp_output[(i + _r) * _C + (j + _r)] = (abs(delta + 1) - abs(delta - 1)) / 2.0f;
		}
	}
}

void CNNArrayCPUv2::computeInitialOutput()
{
	for (size_t i = 0; i < _R; ++i) {
		for (size_t j = 0; j < _C; ++j) {
			float stateVal = _state[i * _C + j];
			_output[i * _C + j] = (abs(stateVal + 1) - abs(stateVal - 1)) / 2.0f;
		}
	}
}

void CNNArrayCPUv2::copyOutput(float* out)
{
	for (size_t i = 0; i < _M; ++i) {
		for (size_t j = 0; j < _N; ++j) {
			out[i* _N + j] = _output[(i + _r) * _C + (j + _r)];
		}
	}
}


void CNNArrayCPUv2::computeInitialOutput2()
{
	for (size_t i = 0; i < _R; ++i) {
		for (size_t j = 0; j < _C; ++j) {
			float stateVal = _output[i * _C + j];
			_output[i * _C + j] = (abs(stateVal + 1) - abs(stateVal - 1)) / 2.0f;
		}
	}
}
