#ifndef CNN_ARRAY_CPU_H
#define CNN_ARRAY_CPU_H

#include <vector>
#include "cnn/cnn.h"

#define CHANGE_THRESHOLD 0.00001
#define COMPARE_THRESHOLD 0.00001

class CNNArrayCPU : public CNN {
private:
	float* _state;
	float* _output;

	size_t _R;
	size_t _C;

public:
	CNNArrayCPU(size_t M, size_t N, size_t r);
	~CNNArrayCPU();

	void run(CNNGene* gene, CNNInput *input, unsigned int N, float dt);
	unsigned int convergenceRun(CNNGene* gene, CNNInput *input, float dt);

	void run(CNNGene* gene, CNNInput *input, unsigned int N);
	unsigned int convergenceRun(CNNGene* gene, CNNInput *input);

	std::vector<long long int> outputAnalysisRun(CNNGene* gene, CNNInput* input, float* idealOutput);

	void copyOutput(float* out);

private:
	bool computeNextState(const float dt);
	bool computeNextState();
	void computeInitialOutput();
	void computeOutput();
	long long int compareOutput(float* ideal);
};

class CNNArrayCPUv2 : public CNN {
private:
	float* _state;
	float* _output;
	float* _tmp_output;

	size_t _R;
	size_t _C;

public:
	CNNArrayCPUv2(size_t M, size_t N, size_t r);
	~CNNArrayCPUv2();

	void run(CNNGene* gene, CNNInput *input, unsigned int N, float dt);
	void run(CNNGene* gene, CNNInput *input, unsigned int N);

	unsigned int convergenceRun(CNNGene* gene, CNNInput *input, float dt) { return 0; }
	unsigned int convergenceRun(CNNGene* gene, CNNInput *input) { return 0; }
	std::vector<long long int> outputAnalysisRun(CNNGene* gene, CNNInput* input, float* idealOutput) { return std::vector<long long int>(); };


	void copyOutput(float* out);

private:
	void computeStateAndOutput(const float dt);
	void computeStateAndOutput();
	void computeInitialOutput();
	void computeInitialOutput2();
};

#endif // CNN_ARRAY_CPU_H
