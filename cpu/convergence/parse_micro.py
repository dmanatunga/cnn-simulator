#! /usr/bin/python
import sys
import csv
import re

GENES = ['ConcentricContour', 'ConnectedComponent', 'CornerDetection', 'EdgeDetection', 'HoleFilling', 'RotationDetector', 'ShadowCreator']
TESTS = ['lowerA', 'capitalA', 'vertLine', 'circle', 'rect', 'filledSquare', 'zero', 'seven', 'eight', 'nine']
DIMS = [1024, 2048]

def parse_results(results_filename):
    results_file = open(results_filename, 'r')
    genes = {}

    line_regex = re.compile(r'(?P<gene>\S+) \(\/?(.+\/)*(?P<test>\S+)_(?P<dim>\d+)x\d+\.dlm\): (?P<time>\d+)')

    dimensions = []
    for line in results_file:
        result = line_regex.match(line)
        if result:
            gene = result.group('gene')
            test = result.group('test')
            dim = int(result.group('dim'))
            time = int(result.group('time'))
            
            if not gene in genes:
                genes[gene] = {}

            gene_data = genes[gene]

            if not test in gene_data:
                gene_data[test] = {}

            test_data = gene_data[test]
            test_data[dim] = time
            
            if not dim in dimensions:
                dimensions.append(dim)
        else:
            print('No match for line %s' % line)
    
    dimensions.sort()
    
    with open('micro_output.csv', 'wb') as outfile:
        writer = csv.writer(outfile)
        
        for gene in GENES:
            header = [gene]

            header.extend(TESTS)
            writer.writerow(header)

            gene_data = genes[gene]
    
            for dim in DIMS:
                row = [dim]
                for test in TESTS:
                    row.append(gene_data[test][dim])
                writer.writerow(row)
            writer.writerow([])
        writer.writerow([])
        writer.writerow([])

        for dim in DIMS:
            header = [dim]
            header.extend(TESTS)
            writer.writerow(header)

            for gene in GENES:
                gene_data = genes[gene]
                row = [gene]

                for test in TESTS:
                    row.append(gene_data[test][dim])
                writer.writerow(row)
            writer.writerow([])


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Incorrect number of input arguments>")
    else:
        results_filename = sys.argv[1]
        parse_results(results_filename)

