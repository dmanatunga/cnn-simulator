declare -a dims=(64 128 256 512 1024 2048 4096)
test=spiral
test_dir=../../tests

echo "Analog v1"
for dim in ${dims[@]} ; do
  echo $dim
  for trial in 1 2 3 4 5 ; do
    ./time-cnn-sim-cpu "HoleFilling" ${test_dir}/${test}_${dim}x${dim}.dlm v1 1000 1
  done
  echo ""
done

echo "Difference Equation v1"
for dim in ${dims[@]} ; do
  echo $dim
  for trial in 1 2 3 4 5 ; do
    ./time-cnn-sim-cpu "HoleFilling" ${test_dir}/${test}_${dim}x${dim}.dlm v1 1000
  done
  echo ""
done

echo "Analog v2"
for dim in ${dims[@]} ; do
  echo $dim
  for trial in 1 2 3 4 5 ; do
    ./time-cnn-sim-cpu "HoleFilling" ${test_dir}/${test}_${dim}x${dim}.dlm v2 1000 1
  done
  echo ""
done

echo "Difference Equation v2"
for dim in ${dims[@]} ; do
  echo $dim
  for trial in 1 2 3 4 5 ; do
    ./time-cnn-sim-cpu "HoleFilling" ${test_dir}/${test}_${dim}x${dim}.dlm v2 1000
  done
  echo ""
done


