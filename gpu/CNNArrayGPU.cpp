#include "CNNArrayGPU.h"
#include "cnn_sim_gpu.h"
#include <string.h>
#include <assert.h>

CNNArrayGPU::CNNArrayGPU(size_t M, size_t N, size_t r)
{
  _M = M;
  _N = N;
  _r = r;

  _R = M + 2 * r;
  _C = N + 2 * r;

  _h_output = new float[_R * _C];

  size_t templatesArrSize = 2 * (2 * _r + 1) * (2 * _r + 1);
  _h_templates = new float[templatesArrSize];
  
  cnn_gpu_init(M, N, r, &_d_state, &_d_output, &_d_u, &_d_templates);
}

CNNArrayGPU::~CNNArrayGPU() 
{
  cnn_gpu_finish(_d_state, _d_output, _d_u, _d_templates);
  delete[] _h_output;
  delete[] _h_templates;
}

void CNNArrayGPU::run(CNNGene* gene, CNNInput* input, unsigned int N_iters, float dt)
{
  assert(gene->r() == _r);
  assert(input->R() == _R);
  assert(input->C() == _C);
   
  size_t templateSize = (2 * _r + 1) * (2 * _r + 1);
  memcpy(_h_templates, gene->A(), sizeof(float) * templateSize);
  memcpy(_h_templates + templateSize, gene->B(), sizeof(float) * templateSize);

  cnn_gpu_run(_M, _N, _r, _h_templates, _d_templates, gene->z(),  
              input->initialState(), _d_state, _d_output, input->u(), _d_u,
              N_iters, dt, _h_output);
  
}

void CNNArrayGPU::run(CNNGene* gene, CNNInput* input, unsigned int N_iters)
{
  assert(gene->r() == _r);
  assert(input->R() == _R);
  assert(input->C() == _C);
   
  size_t templateSize = (2 * _r + 1) * (2 * _r + 1);
  memcpy(_h_templates, gene->A(), sizeof(float) * templateSize);
  memcpy(_h_templates + templateSize, gene->B(), sizeof(float) * templateSize);

  diffeq_cnn_gpu_run(_M, _N, _r, _h_templates, _d_templates, gene->z(),  
                      input->initialState(), _d_state, _d_output, input->u(), _d_u,
                      N_iters, _h_output);
}

void CNNArrayGPU::copyOutput(float* out)
{
  for (size_t i = 0; i < _M; ++i) {
    for (size_t j = 0; j < _N; ++j) {
      out[i* _N + j] = _h_output[(i + _r) * _C + (j + _r)];
    }
  }
}


/////////////////////////////////////////////////////////////////////////////
// Version 2 of CNNArrayGPU - Increased Memory Usage
/////////////////////////////////////////////////////////////////////////////
CNNArrayGPUv2::CNNArrayGPUv2(size_t M, size_t N, size_t r)
{
  _M = M;
  _N = N;
  _r = r;

  _R = M + 2 * r;
  _C = N + 2 * r;

  _h_output = new float[_R * _C];

  size_t templatesArrSize = 2 * (2 * _r + 1) * (2 * _r + 1);
  _h_templates = new float[templatesArrSize];
  
  cnn_gpu_init2(M, N, r, &_d_state, &_d_output, &_d_output2, &_d_u, &_d_templates);
}

CNNArrayGPUv2::~CNNArrayGPUv2() 
{
  cnn_gpu_finish2(_d_state, _d_output, _d_output2, _d_u, _d_templates);
  delete[] _h_output;
  delete[] _h_templates;
}

void CNNArrayGPUv2::run(CNNGene* gene, CNNInput* input, unsigned int N_iters, float dt)
{
  assert(gene->r() == _r);
  assert(input->R() == _R);
  assert(input->C() == _C);
   
  size_t templateSize = (2 * _r + 1) * (2 * _r + 1);
  memcpy(_h_templates, gene->A(), sizeof(float) * templateSize);
  memcpy(_h_templates + templateSize, gene->B(), sizeof(float) * templateSize);

  cnn_gpu_run2(_M, _N, _r, _h_templates, _d_templates, gene->z(),  
              input->initialState(), _d_state, _d_output, _d_output2, input->u(), _d_u,
              N_iters, dt, _h_output);
  
}

void CNNArrayGPUv2::run(CNNGene* gene, CNNInput* input, unsigned int N_iters)
{
  assert(gene->r() == _r);
  assert(input->R() == _R);
  assert(input->C() == _C);
   
  size_t templateSize = (2 * _r + 1) * (2 * _r + 1);
  memcpy(_h_templates, gene->A(), sizeof(float) * templateSize);
  memcpy(_h_templates + templateSize, gene->B(), sizeof(float) * templateSize);

  diffeq_cnn_gpu_run2(_M, _N, _r, _h_templates, _d_templates, gene->z(),  
                      input->initialState(), _d_state, _d_output, _d_output2, input->u(), _d_u,
                      N_iters, _h_output);
}

void CNNArrayGPUv2::copyOutput(float* out)
{
  for (size_t i = 0; i < _M; ++i) {
    for (size_t j = 0; j < _N; ++j) {
      out[i* _N + j] = _h_output[(i + _r) * _C + (j + _r)];
    }
  }
}
