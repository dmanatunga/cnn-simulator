#include "cnn_sim_gpu.h"
#include "cuda_utils.h"


///////////////////////////////////////////////////////////////////////////////
// CNN Setup and Finish Functions
///////////////////////////////////////////////////////////////////////////////
void
cnn_gpu_init(size_t M, size_t N, size_t r, float** d_state, float** d_output, float** d_u, float** d_templates)
{
  size_t R = M + 2 * r;
  size_t C = N + 2 * r;
	size_t N_elems = R * C;
  size_t templates_N = (2 * r + 1) * (2 * r + 1) * 2;

  CUDA_CHECK_ERROR (cudaMalloc ((void**) d_state, N_elems * sizeof (float)));
  CUDA_CHECK_ERROR (cudaMalloc ((void**) d_output, N_elems * sizeof (float)));
  CUDA_CHECK_ERROR (cudaMalloc ((void**) d_u, N_elems * sizeof (float)));
  CUDA_CHECK_ERROR (cudaMalloc ((void**) d_templates, templates_N * sizeof (float)));
}

void
cnn_gpu_init2(size_t M, size_t N, size_t r, float** d_state, float** d_output, float** d_output2, float** d_u, float** d_templates)
{
  size_t R = M + 2 * r;
  size_t C = N + 2 * r;
	size_t N_elems = R * C;
  size_t templates_N = (2 * r + 1) * (2 * r + 1) * 2;

  CUDA_CHECK_ERROR (cudaMalloc ((void**) d_state, N_elems * sizeof (float)));
  CUDA_CHECK_ERROR (cudaMalloc ((void**) d_output, N_elems * sizeof (float)));
  CUDA_CHECK_ERROR (cudaMalloc ((void**) d_output2, N_elems * sizeof (float)));
  CUDA_CHECK_ERROR (cudaMalloc ((void**) d_u, N_elems * sizeof (float)));
  CUDA_CHECK_ERROR (cudaMalloc ((void**) d_templates, templates_N * sizeof (float)));
}

void
cnn_gpu_finish(float* d_state, float* d_output, float* d_u, float* d_templates)
{
	CUDA_CHECK_ERROR (cudaFree (d_state));
	CUDA_CHECK_ERROR (cudaFree (d_output));
	CUDA_CHECK_ERROR (cudaFree (d_u));
	CUDA_CHECK_ERROR (cudaFree (d_templates));
}

void
cnn_gpu_finish2(float* d_state, float* d_output, float* d_output2, float* d_u, float* d_templates)
{
	CUDA_CHECK_ERROR (cudaFree (d_state));
	CUDA_CHECK_ERROR (cudaFree (d_output));
	CUDA_CHECK_ERROR (cudaFree (d_output2));
	CUDA_CHECK_ERROR (cudaFree (d_u));
	CUDA_CHECK_ERROR (cudaFree (d_templates));
}

///////////////////////////////////////////////////////////////////////////////
// CNN State Computation Functions
///////////////////////////////////////////////////////////////////////////////
extern __shared__ float array[];
__global__
void
computeNextState (float* stateArr, const float* __restrict__ outputArr, const float* __restrict__ u, const size_t M, const size_t N, const int r, 
                  const float* __restrict__ A, const float* __restrict__ B, const float z, const float dt, size_t R, size_t C)
{  
	/* block indices */
	int bidx = blockIdx.x;
	int bidy = blockIdx.y;

	/* thread indices */
	int tidx = threadIdx.x;
	int tidy = threadIdx.y;
  
  int x = bidx * BLOCK_SIZE + tidx;
  int y = bidy * BLOCK_SIZE + tidy;
  
  const int shared_width = 2 * r + BLOCK_SIZE;
  float* shared_out = (float*)array;
  float* shared_u = (float*)&array[shared_width * shared_width];
  for (int i = 0; i < shared_width; i += BLOCK_SIZE) {
    int x_loc = x + i;
    int valid_array_row = x < R;

    int x_ind = i + tidx;
    int valid_shared_row = x_ind < shared_width;

    if (valid_array_row && valid_shared_row) {
      for (int j = 0; j < shared_width; j += BLOCK_SIZE) {
        int y_loc = y + j;
        int valid_array_col = y < C;
        
        int y_ind = j + tidy;
        int valid_shared_col = y_ind < shared_width;

        if (valid_array_col && valid_shared_col) {
          shared_out[x_ind * shared_width + y_ind] = outputArr[x_loc * C + y_loc];
          shared_u[x_ind * shared_width + y_ind] = u[x_loc * C + y_loc];
        }
      }
    }
  }
  __syncthreads();

  int mem_loc = (x + r) * C + (y + r);
  float stateDer = z;

  const int template_width = 2 * r + 1; 
  if ((x < M) && (y < N)) {
    float curState = stateArr[mem_loc];
    
    for (int i = 0; i < template_width; ++i) {
      for (int j = 0 ; j < template_width; ++j) {
        stateDer += A[i * template_width + j] * shared_out[(tidx + i) * (shared_width) + (tidy + j)];
        stateDer += B[i * template_width + j] * shared_u[(tidx + i) * (shared_width) + (tidy + j)];      
      }
    }
    stateDer = stateDer - curState;
    stateArr[mem_loc] = curState + stateDer * dt;
  }
}

__global__
void
computeStateAndOutput (float* stateArr, const float* outputArr, const float* u, const size_t M, const size_t N, const int r, 
                           const float* A, const float* B, const float z, const float dt, size_t R, size_t C, float* nextOutputArr)
{
	/* block indices */
	int bidx = blockIdx.x;
	int bidy = blockIdx.y;

	/* thread indices */
	int tidx = threadIdx.x;
	int tidy = threadIdx.y;
  
  int x = bidx * BLOCK_SIZE + tidx;
  int y = bidy * BLOCK_SIZE + tidy;
  
  const int shared_width = 2 * r + BLOCK_SIZE;
  float* shared_out = (float*)array;
  float* shared_u = (float*)&array[shared_width * shared_width];
  for (int i = 0; i < shared_width; i += BLOCK_SIZE) {
    int x_loc = x + i;
    int valid_array_row = x < R;

    int x_ind = i + tidx;
    int valid_shared_row = x_ind < shared_width;

    if (valid_array_row && valid_shared_row) {
      for (int j = 0; j < shared_width; j += BLOCK_SIZE) {
        int y_loc = y + j;
        int valid_array_col = y < C;
        
        int y_ind = j + tidy;
        int valid_shared_col = y_ind < shared_width;

        if (valid_array_col && valid_shared_col) {
          shared_out[x_ind * shared_width + y_ind] = outputArr[x_loc * C + y_loc];
          shared_u[x_ind * shared_width + y_ind] = u[x_loc * C + y_loc];
        }
      }
    }
  }
  __syncthreads();
 
  int mem_loc = (x + r) * C + (y + r);
  float stateDer = z;

  
  const int template_width = 2 * r + 1; 
  if ((x < M) && (y < N)) {
    float curState = stateArr[mem_loc];
    
    for (int i = 0; i < template_width; ++i) {
      for (int j = 0 ; j < template_width; ++j) {
        stateDer += A[i * template_width + j] * shared_out[(tidx + i) * (shared_width) + (tidy + j)];
        stateDer += B[i * template_width + j] * shared_u[(tidx + i) * (shared_width) + (tidy + j)];      
      }
    }
    stateDer = stateDer - curState;
    float newState = curState + stateDer * dt;
    stateArr[mem_loc] = newState;
    float output = (fabsf(newState + 1.0f) - fabsf(newState - 1.0f)) / 2.0f;
    nextOutputArr[mem_loc] = output;
  }
}

// Difference Equation CNN Implementation
__global__
void
diffeq_computeNextState (float* stateArr, const float* outputArr, const float* u, const size_t M, const size_t N, const int r, 
                  const float* A, const float* B, const float z, size_t R, size_t C)
{
	/* block indices */
	int bidx = blockIdx.x;
	int bidy = blockIdx.y;

	/* thread indices */
	int tidx = threadIdx.x;
	int tidy = threadIdx.y;
  
  int x = bidx * BLOCK_SIZE + tidx;
  int y = bidy * BLOCK_SIZE + tidy;
  
  const int shared_width = 2 * r + BLOCK_SIZE;
  float* shared_out = (float*)array;
  float* shared_u = (float*)&array[shared_width * shared_width];
  for (int i = 0; i < shared_width; i += BLOCK_SIZE) {
    int x_loc = x + i;
    int valid_array_row = x < R;

    int x_ind = i + tidx;
    int valid_shared_row = x_ind < shared_width;

    if (valid_array_row && valid_shared_row) {
      for (int j = 0; j < shared_width; j += BLOCK_SIZE) {
        int y_loc = y + j;
        int valid_array_col = y < C;
        
        int y_ind = j + tidy;
        int valid_shared_col = y_ind < shared_width;

        if (valid_array_col && valid_shared_col) {
          shared_out[x_ind * shared_width + y_ind] = outputArr[x_loc * C + y_loc];
          shared_u[x_ind * shared_width + y_ind] = u[x_loc * C + y_loc];
        }
      }
    }
  }
  __syncthreads();

  int mem_loc = (x + r) * C + (y + r);
  float newState = z;

  const int template_width = 2 * r + 1; 
  if ((x < M) && (y < N)) {
    
    for (int i = 0; i < template_width; ++i) {
      for (int j = 0 ; j < template_width; ++j) {
        newState += A[i * template_width + j] * shared_out[(tidx + i) * (shared_width) + (tidy + j)];
        newState += B[i * template_width + j] * shared_u[(tidx + i) * (shared_width) + (tidy + j)];      
      }
    }
    stateArr[mem_loc] = newState;
  }
}

__global__
void
diffeq_computeStateAndOutput (float* stateArr, const float* outputArr, const float* u, const size_t M, const size_t N, const int r, 
                  const float* A, const float* B, const float z, size_t R, size_t C, float* nextOutputArr)
{
	/* block indices */
	int bidx = blockIdx.x;
	int bidy = blockIdx.y;

	/* thread indices */
	int tidx = threadIdx.x;
	int tidy = threadIdx.y;
  
  int x = bidx * BLOCK_SIZE + tidx;
  int y = bidy * BLOCK_SIZE + tidy;
  
  const int shared_width = 2 * r + BLOCK_SIZE;
  float* shared_out = (float*)array;
  float* shared_u = (float*)&array[shared_width * shared_width];
  for (int i = 0; i < shared_width; i += BLOCK_SIZE) {
    int x_loc = x + i;
    int valid_array_row = x < R;

    int x_ind = i + tidx;
    int valid_shared_row = x_ind < shared_width;

    if (valid_array_row && valid_shared_row) {
      for (int j = 0; j < shared_width; j += BLOCK_SIZE) {
        int y_loc = y + j;
        int valid_array_col = y < C;
        
        int y_ind = j + tidy;
        int valid_shared_col = y_ind < shared_width;

        if (valid_array_col && valid_shared_col) {
          shared_out[x_ind * shared_width + y_ind] = outputArr[x_loc * C + y_loc];
          shared_u[x_ind * shared_width + y_ind] = u[x_loc * C + y_loc];
        }
      }
    }
  }
  __syncthreads();

  int mem_loc = (x + r) * C + (y + r);
  float newState = z;

  const int template_width = (2 * r + 1);
  if ((x < M) && (y < N)) {
    
    for (int i = 0; i < template_width; ++i) {
      for (int j = 0 ; j < template_width; ++j) {
        newState += A[i * template_width + j] * shared_out[(tidx + i) * (shared_width) + (tidy + j)];
        newState += B[i * template_width + j] * shared_u[(tidx + i) * (shared_width) + (tidy + j)];      
      }
    }

    stateArr[mem_loc] = newState;
    float output = (fabsf(newState + 1.0f) - fabsf(newState - 1.0f)) / 2.0f;
    nextOutputArr[mem_loc] = output;
  }
}

__global__
void
diffeq_computeStateAndOutput2 (const float* outputArr, const float* u, const size_t M, const size_t N, const int r, 
                  const float* A, const float* B, const float z, size_t R, size_t C, float* nextOutputArr)
{
	/* block indices */
	int bidx = blockIdx.x;
	int bidy = blockIdx.y;

	/* thread indices */
	int tidx = threadIdx.x;
	int tidy = threadIdx.y;
  
  int x = bidx * BLOCK_SIZE + tidx;
  int y = bidy * BLOCK_SIZE + tidy;
  
  const int shared_width = 2 * r + BLOCK_SIZE;
  float* shared_out = (float*)array;
  float* shared_u = (float*)&array[shared_width * shared_width];
  for (int i = 0; i < shared_width; i += BLOCK_SIZE) {
    int x_loc = x + i;
    int valid_array_row = x < R;

    int x_ind = i + tidx;
    int valid_shared_row = x_ind < shared_width;

    if (valid_array_row && valid_shared_row) {
      for (int j = 0; j < shared_width; j += BLOCK_SIZE) {
        int y_loc = y + j;
        int valid_array_col = y < C;
        
        int y_ind = j + tidy;
        int valid_shared_col = y_ind < shared_width;

        if (valid_array_col && valid_shared_col) {
          shared_out[x_ind * shared_width + y_ind] = outputArr[x_loc * C + y_loc];
          shared_u[x_ind * shared_width + y_ind] = u[x_loc * C + y_loc];
        }
      }
    }
  }
  __syncthreads();

  int mem_loc = (x + r) * C + (y + r);
  float newState = z;

  const int template_width = (2 * r + 1);
  if ((x < M) && (y < N)) {
    
    for (int i = 0; i < template_width; ++i) {
      for (int j = 0 ; j < template_width; ++j) {
        newState += A[i * template_width + j] * shared_out[(tidx + i) * (shared_width) + (tidy + j)];
        newState += B[i * template_width + j] * shared_u[(tidx + i) * (shared_width) + (tidy + j)];      
      }
    }
    float output = (fabsf(newState + 1.0f) - fabsf(newState - 1.0f)) / 2.0f;
    nextOutputArr[mem_loc] = output;
  }
}
///////////////////////////////////////////////////////////////////////////////
// CNN Output Computation Functions
///////////////////////////////////////////////////////////////////////////////
__global__
void
computeInitialOutput (const float* stateArr, float* outputArr, size_t R, size_t C) 
{
	/* block indices */
	int bidx = blockIdx.x;
	int bidy = blockIdx.y;

	/* thread indices */
	int tidx = threadIdx.x;
	int tidy = threadIdx.y;

  int x = bidx * BLOCK_SIZE + tidx;
  int y = bidy * BLOCK_SIZE + tidy;
  int mem_loc = x * C + y;

  if ((x < R) && (y < C)) {
    float state = stateArr[mem_loc];
    float output = (fabsf(state + 1.0f) - fabsf(state - 1.0f)) / 2.0f;
    outputArr[mem_loc] = output;
  }
}

__global__
void
computeInitialOutput2 (float* outputArr, size_t R, size_t C) 
{
	/* block indices */
	int bidx = blockIdx.x;
	int bidy = blockIdx.y;

	/* thread indices */
	int tidx = threadIdx.x;
	int tidy = threadIdx.y;

  int x = bidx * BLOCK_SIZE + tidx;
  int y = bidy * BLOCK_SIZE + tidy;
  int mem_loc = x * C + y;

  if ((x < R) && (y < C)) {
    float state = outputArr[mem_loc];
    float output = (fabsf(state + 1.0f) - fabsf(state - 1.0f)) / 2.0f;
    outputArr[mem_loc] = output;
  }
}

__global__
void
computeOutput (const float* stateArr, float* outputArr, size_t M, size_t N, size_t r, size_t C) 
{
	/* block indices */
	int bidx = blockIdx.x;
	int bidy = blockIdx.y;

	/* thread indices */
	int tidx = threadIdx.x;
	int tidy = threadIdx.y;
  
  int x = bidx * BLOCK_SIZE + tidx;
  int y = bidy * BLOCK_SIZE + tidy;
  int mem_loc = (x + r) * C + (y + r);
  
  if ((x < M) && (y < N)) {
    float state = stateArr[mem_loc];
    float output = (fabsf(state + 1.0f) - fabsf(state - 1.0f)) / 2.0f;
    outputArr[mem_loc] = output;
  }
}

///////////////////////////////////////////////////////////////////////////////
// CNN Running Functions
///////////////////////////////////////////////////////////////////////////////
void
run_cnn (size_t M, size_t N, size_t r, float* stateArr, float* outputArr, const float* u, const float* A, const float* B, const float z, unsigned int N_iters, float dt)
{
  size_t R = M + 2 * r;
  size_t C = N + 2 * r;
	size_t rBlocks = (R + (BLOCK_SIZE - 1)) / BLOCK_SIZE;
	size_t cBlocks = (C + (BLOCK_SIZE - 1)) / BLOCK_SIZE;
  size_t shared_mem_size = (2 * r + BLOCK_SIZE) * (2 * r + BLOCK_SIZE) * 2 * sizeof(float);

  dim3 initGrid (rBlocks, cBlocks);	
	dim3 block (BLOCK_SIZE, BLOCK_SIZE);	
  computeInitialOutput <<< initGrid, block >>> (stateArr, outputArr, R, C);

	unsigned int mBlocks = (M + (BLOCK_SIZE - 1)) / BLOCK_SIZE;
	unsigned int nBlocks = (N + (BLOCK_SIZE - 1)) / BLOCK_SIZE;
  dim3 grid (mBlocks, nBlocks);
 
	cudaThreadSynchronize ();

  for (unsigned int i = 1; i <= N_iters; ++i) {
    computeNextState <<< grid, block, shared_mem_size >>> (stateArr, outputArr, u, M, N, r, A, B, z, dt, R, C);
    cudaThreadSynchronize();

    computeOutput <<< grid, block >>> (stateArr, outputArr, M, N, r, C);
    cudaThreadSynchronize(); 
  }
}

void
run_cnn2 (size_t M, size_t N, size_t r, float* stateArr, float* outputArr, float* outputArr2, const float* u, const float* A, const float* B, const float z, unsigned int N_iters, float dt)
{
  size_t R = M + 2 * r;
  size_t C = N + 2 * r;
	size_t rBlocks = (R + (BLOCK_SIZE - 1)) / BLOCK_SIZE;
	size_t cBlocks = (C + (BLOCK_SIZE - 1)) / BLOCK_SIZE;
  size_t shared_mem_size = (2 * r + BLOCK_SIZE) * (2 * r + BLOCK_SIZE) * 2 * sizeof(float);
	

  dim3 initGrid (rBlocks, cBlocks);	
	dim3 block (BLOCK_SIZE, BLOCK_SIZE);	
  computeInitialOutput <<< initGrid, block >>> (stateArr, outputArr, R, C);

	unsigned int mBlocks = (M + (BLOCK_SIZE - 1)) / BLOCK_SIZE;
	unsigned int nBlocks = (N + (BLOCK_SIZE - 1)) / BLOCK_SIZE;
  dim3 grid (mBlocks, nBlocks);
 
	cudaThreadSynchronize ();

  for (unsigned int i = 1; i <= N_iters; ++i) {
    computeStateAndOutput <<< grid, block, shared_mem_size >>> (stateArr, outputArr, u, M, N, r, A, B, z, dt, R, C, outputArr2);
    float* tmp = outputArr;
    outputArr = outputArr2;
    outputArr2 = tmp;
    cudaThreadSynchronize();
  }
}


void
diffeq_run_cnn (size_t M, size_t N, size_t r, float* stateArr, float* outputArr, const float* u, const float* A, const float* B, const float z, unsigned int N_iters)
{
  size_t R = M + 2 * r;
  size_t C = N + 2 * r;
	size_t rBlocks = (R + (BLOCK_SIZE - 1)) / BLOCK_SIZE;
	size_t cBlocks = (C + (BLOCK_SIZE - 1)) / BLOCK_SIZE;
  size_t shared_mem_size = (2 * r + BLOCK_SIZE) * (2 * r + BLOCK_SIZE) * 2 * sizeof(float);
	

  dim3 initGrid (rBlocks, cBlocks);	
	dim3 block (BLOCK_SIZE, BLOCK_SIZE);	
  computeInitialOutput <<< initGrid, block >>> (stateArr, outputArr, R, C);

	unsigned int mBlocks = (M + (BLOCK_SIZE - 1)) / BLOCK_SIZE;
	unsigned int nBlocks = (N + (BLOCK_SIZE - 1)) / BLOCK_SIZE;
  dim3 grid (mBlocks, nBlocks);
 
	cudaThreadSynchronize ();

  for (unsigned int i = 1; i <= N_iters; ++i) {
    diffeq_computeNextState <<< grid, block, shared_mem_size >>> (stateArr, outputArr, u, M, N, r, A, B, z, R, C);
    cudaThreadSynchronize();

    computeOutput <<< grid, block >>> (stateArr, outputArr, M, N, r, C);
    cudaThreadSynchronize(); 
  }
}


void
diffeq_run_cnn2 (size_t M, size_t N, size_t r, float* stateArr, float* outputArr, float* outputArr2, const float* u, const float* A, const float* B, const float z, unsigned int N_iters)
{
  size_t R = M + 2 * r;
  size_t C = N + 2 * r;
	size_t rBlocks = (R + (BLOCK_SIZE - 1)) / BLOCK_SIZE;
	size_t cBlocks = (C + (BLOCK_SIZE - 1)) / BLOCK_SIZE;
  size_t shared_mem_size = (2 * r + BLOCK_SIZE) * (2 * r + BLOCK_SIZE) * 2 * sizeof(float);
	

  dim3 initGrid (rBlocks, cBlocks);	
	dim3 block (BLOCK_SIZE, BLOCK_SIZE);	
  computeInitialOutput2 <<< initGrid, block >>> (outputArr, R, C);

	unsigned int mBlocks = (M + (BLOCK_SIZE - 1)) / BLOCK_SIZE;
	unsigned int nBlocks = (N + (BLOCK_SIZE - 1)) / BLOCK_SIZE;
  dim3 grid (mBlocks, nBlocks);
 
	cudaThreadSynchronize ();

  for (unsigned int i = 1; i <= N_iters; ++i) {
    diffeq_computeStateAndOutput2 <<< grid, block, shared_mem_size >>> (outputArr, u, M, N, r, A, B, z, R, C, outputArr2);
    float* tmp = outputArr;
    outputArr = outputArr2;
    outputArr2 = tmp;
    cudaThreadSynchronize();
  }
}

///////////////////////////////////////////////////////////////////////////////
// Outside Visible API Calls
///////////////////////////////////////////////////////////////////////////////
void
cnn_gpu_run(size_t M, size_t N, size_t r, float* h_templates, float* d_templates, float z, 
            float* h_state, float* d_state, float* d_output, float* h_u, float* d_u,
            unsigned int N_iters, float dt, float* h_output)
{
size_t R = M + 2 * r;
  size_t C = N + 2 * r;
	size_t N_elems = R * C;
  size_t template_size = (2 * r + 1) * (2 * r + 1);
  size_t templates_N = 2 * template_size;

	CUDA_CHECK_ERROR (cudaMemcpy (d_state, h_state, N_elems * sizeof (float), 
																cudaMemcpyHostToDevice));
	CUDA_CHECK_ERROR (cudaMemcpy (d_u, h_u, N_elems * sizeof (float), 
																cudaMemcpyHostToDevice));
	CUDA_CHECK_ERROR (cudaMemcpy (d_templates, h_templates, templates_N * sizeof (float), 
																cudaMemcpyHostToDevice));

  const float* A = d_templates;	
  const float* B = d_templates + template_size;

#ifdef TIME_CNN_SIM
	cudaEvent_t start, stop;
	CUDA_CHECK_ERROR (cudaEventCreate (&start));
	CUDA_CHECK_ERROR (cudaEventCreate (&stop));
	float elapsedTime;
	CUDA_CHECK_ERROR (cudaEventRecord (start, 0));
#endif

  run_cnn (M, N, r, d_state, d_output, d_u, A, B, z, N_iters, dt);

#ifdef TIME_CNN_SIM
  CUDA_CHECK_ERROR (cudaEventRecord (stop, 0));
	CUDA_CHECK_ERROR (cudaEventSynchronize (stop));
	CUDA_CHECK_ERROR (cudaEventElapsedTime (&elapsedTime, start, stop));
	elapsedTime = elapsedTime / 5;

	fprintf (stdout, "Time (ms): %f\n", elapsedTime);

	CUDA_CHECK_ERROR (cudaEventDestroy (start));
	CUDA_CHECK_ERROR (cudaEventDestroy (stop));
#endif
	
  CUDA_CHECK_ERROR (cudaMemcpy (h_output, d_output, N_elems * sizeof (float), 
																cudaMemcpyDeviceToHost));

}


void
cnn_gpu_run2(size_t M, size_t N, size_t r, float* h_templates, float* d_templates, float z, 
            float* h_state, float* d_state, float* d_output, float* d_output2, float* h_u, float* d_u,
            unsigned int N_iters, float dt, float* h_output)
{
  size_t R = M + 2 * r;
  size_t C = N + 2 * r;
	size_t N_elems = R * C;
  size_t template_size = (2 * r + 1) * (2 * r + 1);
  size_t templates_N = 2 * template_size;

	CUDA_CHECK_ERROR (cudaMemcpy (d_state, h_state, N_elems * sizeof (float), 
																cudaMemcpyHostToDevice));
	CUDA_CHECK_ERROR (cudaMemcpy (d_u, h_u, N_elems * sizeof (float), 
																cudaMemcpyHostToDevice));
	CUDA_CHECK_ERROR (cudaMemcpy (d_templates, h_templates, templates_N * sizeof (float), 
																cudaMemcpyHostToDevice));

  const float* A = d_templates;	
  const float* B = d_templates + template_size;

#ifdef TIME_CNN_SIM
	cudaEvent_t start, stop;
	CUDA_CHECK_ERROR (cudaEventCreate (&start));
	CUDA_CHECK_ERROR (cudaEventCreate (&stop));
	float elapsedTime;
	CUDA_CHECK_ERROR (cudaEventRecord (start, 0));
#endif

  run_cnn2 (M, N, r, d_state, d_output, d_output2, d_u, A, B, z, N_iters, dt);

#ifdef TIME_CNN_SIM
  CUDA_CHECK_ERROR (cudaEventRecord (stop, 0));
	CUDA_CHECK_ERROR (cudaEventSynchronize (stop));
	CUDA_CHECK_ERROR (cudaEventElapsedTime (&elapsedTime, start, stop));
	elapsedTime = elapsedTime / 5;
	fprintf (stdout, "Time (ms): %f\n", elapsedTime);

	CUDA_CHECK_ERROR (cudaEventDestroy (start));
	CUDA_CHECK_ERROR (cudaEventDestroy (stop));
#endif

  if (N_iters % 2) {
    CUDA_CHECK_ERROR (cudaMemcpy (h_output, d_output2, N_elems * sizeof (float), 
                                  cudaMemcpyDeviceToHost));
  } else {
    CUDA_CHECK_ERROR (cudaMemcpy (h_output, d_output, N_elems * sizeof (float), 
                                  cudaMemcpyDeviceToHost));
  }

}

void
diffeq_cnn_gpu_run(size_t M, size_t N, size_t r, float* h_templates, float* d_templates, float z, 
            float* h_state, float* d_state, float* d_output, float* h_u, float* d_u,
            unsigned int N_iters, float* h_output)
{

  size_t R = M + 2 * r;
  size_t C = N + 2 * r;
	size_t N_elems = R * C;
  size_t template_size = (2 * r + 1) * (2 * r + 1);
  size_t templates_N = 2 * template_size;

	CUDA_CHECK_ERROR (cudaMemcpy (d_state, h_state, N_elems * sizeof (float), 
																cudaMemcpyHostToDevice));
	CUDA_CHECK_ERROR (cudaMemcpy (d_u, h_u, N_elems * sizeof (float), 
																cudaMemcpyHostToDevice));
	CUDA_CHECK_ERROR (cudaMemcpy (d_templates, h_templates, templates_N * sizeof (float), 
																cudaMemcpyHostToDevice));

  const float* A = d_templates;	
  const float* B = d_templates + template_size;

#ifdef TIME_CNN_SIM
	cudaEvent_t start, stop;
	CUDA_CHECK_ERROR (cudaEventCreate (&start));
	CUDA_CHECK_ERROR (cudaEventCreate (&stop));
	float elapsedTime;
	CUDA_CHECK_ERROR (cudaEventRecord (start, 0));
#endif

  diffeq_run_cnn (M, N, r, d_state, d_output, d_u, A, B, z, N_iters);

#ifdef TIME_CNN_SIM
  CUDA_CHECK_ERROR (cudaEventRecord (stop, 0));
	CUDA_CHECK_ERROR (cudaEventSynchronize (stop));
	CUDA_CHECK_ERROR (cudaEventElapsedTime (&elapsedTime, start, stop));
	elapsedTime = elapsedTime / 5;

	fprintf (stdout, "Time (ms): %f\n", elapsedTime);

	CUDA_CHECK_ERROR (cudaEventDestroy (start));
	CUDA_CHECK_ERROR (cudaEventDestroy (stop));
#endif
	
  CUDA_CHECK_ERROR (cudaMemcpy (h_output, d_output, N_elems * sizeof (float), 
																cudaMemcpyDeviceToHost));
}

void
diffeq_cnn_gpu_run2(size_t M, size_t N, size_t r, float* h_templates, float* d_templates, float z, 
            float* h_state, float* d_state, float* d_output, float* d_output2, float* h_u, float* d_u,
            unsigned int N_iters, float* h_output)
{

  size_t R = M + 2 * r;
  size_t C = N + 2 * r;
	size_t N_elems = R * C;
  size_t template_size = (2 * r + 1) * (2 * r + 1);
  size_t templates_N = 2 * template_size;

	CUDA_CHECK_ERROR (cudaMemcpy (d_state, h_state, N_elems * sizeof (float), 
																cudaMemcpyHostToDevice));
	CUDA_CHECK_ERROR (cudaMemcpy (d_u, h_u, N_elems * sizeof (float), 
																cudaMemcpyHostToDevice));
	CUDA_CHECK_ERROR (cudaMemcpy (d_templates, h_templates, templates_N * sizeof (float), 
																cudaMemcpyHostToDevice));

  const float* A = d_templates;	
  const float* B = d_templates + template_size;

#ifdef TIME_CNN_SIM
	cudaEvent_t start, stop;
	CUDA_CHECK_ERROR (cudaEventCreate (&start));
	CUDA_CHECK_ERROR (cudaEventCreate (&stop));
	float elapsedTime;
	CUDA_CHECK_ERROR (cudaEventRecord (start, 0));
#endif

  diffeq_run_cnn2 (M, N, r, d_state, d_output, d_output2, d_u, A, B, z, N_iters);

#ifdef TIME_CNN_SIM
  CUDA_CHECK_ERROR (cudaEventRecord (stop, 0));
	CUDA_CHECK_ERROR (cudaEventSynchronize (stop));
	CUDA_CHECK_ERROR (cudaEventElapsedTime (&elapsedTime, start, stop));
	elapsedTime = elapsedTime / 5;

	fprintf (stdout, "GPU Time (ms): %f\n", elapsedTime);

	CUDA_CHECK_ERROR (cudaEventDestroy (start));
	CUDA_CHECK_ERROR (cudaEventDestroy (stop));
#endif

  if (N_iters % 2) {
    CUDA_CHECK_ERROR (cudaMemcpy (h_output, d_output2, N_elems * sizeof (float), 
                                  cudaMemcpyDeviceToHost));
  } else {
    CUDA_CHECK_ERROR (cudaMemcpy (h_output, d_output, N_elems * sizeof (float), 
                                  cudaMemcpyDeviceToHost));
  }
}
