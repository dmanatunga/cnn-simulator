#! /usr/bin/python
import os
import sys
import shutil
import re

IDEAL_DATA = {'ShadowCreator': {1024: {'seven': 1027, 'nine': 1027, 'vertLine': 1025, 'zero': 1027, 'filledSquare': 1027, 'lowerA': 1027, 'eight': 1027, 'capitalA': 1027, 'circle': 1027, 'rect': 1027}, 2048: {'seven': 2051, 'nine': 2051, 'vertLine': 2049, 'zero': 2051, 'filledSquare': 2051, 'lowerA': 2051, 'eight': 2051, 'capitalA': 2051, 'circle': 2051, 'rect': 2051}}, 'ConnectedComponent': {1024: {'seven': 1618, 'nine': 1662, 'vertLine': 2046, 'zero': 1654, 'filledSquare': 2044, 'lowerA': 1674, 'eight': 1650, 'capitalA': 1874, 'circle': 1856, 'rect': 1880}, 2048: {'seven': 3194, 'nine': 3284, 'vertLine': 4094, 'zero': 3270, 'filledSquare': 4092, 'lowerA': 3266, 'eight': 3260, 'capitalA': 3634, 'circle': 3688, 'rect': 3674}}, 'HoleFilling': {1024: {'seven': 646, 'nine': 816, 'vertLine': 515, 'zero': 244, 'filledSquare': 4, 'lowerA': 720, 'eight': 392, 'capitalA': 337, 'circle': 221, 'rect': 99}, 2048: {'seven': 1279, 'nine': 1629, 'vertLine': 1027, 'zero': 494, 'filledSquare': 4, 'lowerA': 1387, 'eight': 795, 'capitalA': 728, 'circle': 446, 'rect': 269}}, 'CornerDetection': {1024: {'seven': 3, 'nine': 3, 'vertLine': 3, 'zero': 3, 'filledSquare': 3, 'lowerA': 3, 'eight': 3, 'capitalA': 3, 'circle': 3, 'rect': 3}, 2048: {'seven': 3, 'nine': 3, 'vertLine': 3, 'zero': 3, 'filledSquare': 3, 'lowerA': 3, 'eight': 3, 'capitalA': 3, 'circle': 3, 'rect': 3}}, 'RotationDetector': {1024: {'seven': 835, 'nine': 448, 'vertLine': 515, 'zero': 444, 'filledSquare': 2, 'lowerA': 530, 'eight': 413, 'capitalA': 496, 'circle': 438, 'rect': 2}, 2048: {'seven': 1656, 'nine': 921, 'vertLine': 1027, 'zero': 889, 'filledSquare': 2, 'lowerA': 1168, 'eight': 830, 'capitalA': 1134, 'circle': 889, 'rect': 2}}, 'EdgeDetection': {1024: {'seven': 2, 'nine': 2, 'vertLine': 2, 'zero': 2, 'filledSquare': 2, 'lowerA': 2, 'eight': 2, 'capitalA': 2, 'circle': 2, 'rect': 2}, 2048: {'seven': 2, 'nine': 2, 'vertLine': 2, 'zero': 2, 'filledSquare': 2, 'lowerA': 2, 'eight': 2, 'capitalA': 2, 'circle': 2, 'rect': 2}}, 'ConcentricContour': {1024: {'seven': 84, 'nine': 94, 'vertLine': 2, 'zero': 77, 'filledSquare': 512, 'lowerA': 95, 'eight': 82, 'capitalA': 88, 'circle': 8, 'rect': 6}, 2048: {'seven': 162, 'nine': 183, 'vertLine': 2, 'zero': 148, 'filledSquare': 1024, 'lowerA': 170, 'eight': 159, 'capitalA': 156, 'circle': 8, 'rect': 6}}}


SIM_TEST_DIR = '../../test_files'

GENES = ['ConcentricContour', 'ConnectedComponent', 'CornerDetection', 'EdgeDetection', 'HoleFilling', 'RotationDetector', 'ShadowCreator']
TESTS = ['lowerA', 'capitalA', 'vertLine', 'circle', 'rect', 'filledSquare', 'zero', 'seven', 'eight', 'nine']
DIMS = [1024, 2048]
BIN = 'gpu-timing'

RESULTS_DIR = 'results'
OVERALL_FILE = 'overall.csv'
STATS_FILE = 'stats.csv'

def process_file(filename):
    gpu_time = re.compile(r'GPU Time \(ms\): (?P<time_ms>((\d+\.\d+(e-?\d+)?)|(\d+)))')
    total_time = re.compile(r'Total Time \(ms\): (?P<time_ms>((\d+\.\d+(e-?\d+)?)|(\d+)))')
    with open(filename, 'r') as results_file:
        data = {'gpu_times': [], 'total_times': []}
        for line in results_file:
            result = gpu_time.match(line)
            if result:
                data['gpu_times'].append(float(result.group('time_ms')))

            result = total_time.match(line)
            if result:
                data['total_times'].append(float(result.group('time_ms')))
        return data

def write_overall_data(data, results_dir):
    output_filename = os.path.join(results_dir, OVERALL_FILE)
    import csv
    with open(output_filename, 'w') as output_file:
        output_writer = csv.writer(output_file)

        for gene in GENES:
            for dim in DIMS:
                output_writer.writerow(['%s - %d GPU Times' % (gene, dim)])
                for test in TESTS:
                    row = [test]
                    row.extend(data[dim][gene][test]['gpu_times'])
                    output_writer.writerow(row)
                output_writer.writerow([])

                output_writer.writerow(['%s - %d Total Times' % (gene, dim)])
                for test in TESTS:
                    row = [test]
                    row.extend(data[dim][gene][test]['total_times'])
                    output_writer.writerow(row)
                output_writer.writerow([])
            output_writer.writerow([])

    output_filename = os.path.join(results_dir, STATS_FILE)
    import numpy
    with open(output_filename, 'w') as output_file:
        output_writer = csv.writer(output_file)

        for gene in GENES:
            for dim in DIMS:
                header = ['%s - %d GPU Times' % (gene, test)]
                header.extend(['Avg. (drop min-max)', 'Avg', 'Min', 'Max'])
                output_writer.writerow(header)
                for test in TESTS:
                    row = [test]
                    times = data[dim][gene][test]['gpu_times']
                    min_time = min(times)
                    max_time = max(times)
                    sum_time = sum(times)
                    avg_time = numpy.mean(times)
                    n = len(times)
                    if n <= 2:
                        avg_drop = 'N/A'
                    else:
                        avg_drop = (sum_time - max_time - mini_time) / (len(times) - 2)
                    
                    row.extend([avg_drop, avg_time, min_time, max_time])
                    output_writer.writerow(row)
                output_writer.writerow([])

                header = ['%s - %d Total Times' % (gene, test)]
                header.extend(['Avg. (drop min-max)', 'Avg', 'Min', 'Max'])
                output_writer.writerow(header)
                for test in TESTS:
                    row = [test]
                    times = data[dim][gene][test]['total_times']
                    min_time = min(times)
                    max_time = max(times)
                    sum_time = sum(times)
                    avg_time = numpy.mean(times)
                    n = len(times)
                    if n <= 2:
                        avg_drop = 'N/A'
                    else:
                        avg_drop = (sum_time - max_time - mini_time) / (len(times) - 2)
                    
                    row.extend([avg_drop, avg_time, min_time, max_time])
                    output_writer.writerow(row)
                output_writer.writerow([])
            output_writer.writerow([])



def run_tests():
    results_dir = os.path.join(os.getcwd(), RESULTS_DIR)
    if os.path.exists(results_dir):
        print('Erasing results directory.')
        shutil.rmtree(results_dir)

    os.makedirs(results_dir)

    num_tests = len(GENES) * len(TESTS) * len(DIMS)
    test_id = 0
    data = {}
    for dim in DIMS:
        dim_data = {}
        data[dim] = dim_data
        for gene in GENES:
            gene_data = {}
            dim_data[gene] = gene_data
            for test in TESTS:
                test_id = test_id + 1
                test_name = '%s_%s_%dx%d' % (gene, test, dim, dim)
                print('Running %s (%d/%d)...' % (test_name, test_id, num_tests))
                test_file = '%s/%s_%dx%d.dlm' % (SIM_TEST_DIR, test, dim, dim)
                output_file = os.path.join(results_dir, '%s.out' % test_name)
                os.system('./%s %s %s %d > %s' % (BIN, gene, test_file, IDEAL_DATA[gene][dim][test], output_file))
                test_data = process_file(output_file)
                gene_data[test] = test_data

    print('All tests completed.')
    write_overall_data(data, results_dir)

if __name__ == "__main__":
    run_tests()
