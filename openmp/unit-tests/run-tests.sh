
declare -a tests=(vertLine rect circle spiral filledSquare lowerA capitalA zero seven eight nine)
dim=128

for i in ${tests[@]} ; do
  echo $i
  ./test-cnn-sim-omp "HoleFilling" tests/${i}_${dim}x${dim}.dlm tests/out_${i}_${dim}x${dim}.dlm 50 1
  echo ""
done

