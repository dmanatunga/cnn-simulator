#ifndef CNN_ARRAY_OPENMP_SIMD_H
#define CNN_ARRAY_OPENMP_SIMD_H

#include "cnn/cnn.h"

class CNNArrayOpenMPSIMD : public CNN {
  private:
    float* _state;
    float* _output;

    size_t _R;
    size_t _C;

  public:
    CNNArrayOpenMPSIMD(size_t M, size_t N, size_t r);   
    ~CNNArrayOpenMPSIMD();

    void run(CNNGene* gene, CNNInput *input, unsigned int N, float dt);
    void run(CNNGene* gene, CNNInput *input, unsigned int N);
    void copyOutput(float* out);

  private:
    void computeNextState(float dt);
    void computeNextState();
    void computeInitialOutput();
    void computeOutput();
};

class CNNArrayOpenMPSIMDv2 : public CNN {
  private:
    float* _state;
    float* _output;
    float* _tmp_output;

    size_t _R;
    size_t _C;

  public:
    CNNArrayOpenMPSIMDv2(size_t M, size_t N, size_t r);   
    ~CNNArrayOpenMPSIMDv2();

    void run(CNNGene* gene, CNNInput *input, unsigned int N, float dt);
    void run(CNNGene* gene, CNNInput *input, unsigned int N);
    void copyOutput(float* out);

  private:
    void computeStateAndOutput(float dt);
    void computeStateAndOutput();
    void computeInitialOutput();
    void computeInitialOutput2();
};
#endif // CNN_ARRAY_OPENMP_SIMD_H
