declare -a dims=(64 128 256)
test=spiral
test_dir=../../tests

echo "Analog v1"
for dim in ${dims[@]} ; do
  echo $dim
  ./time-cnn-sim-openmp-simd "HoleFilling" ${test_dir}/${test}_${dim}x${dim}.dlm v1 1000 1
  echo ""
done

echo "Difference Equation v1"
for dim in ${dims[@]} ; do
  echo $dim
  ./time-cnn-sim-openmp-simd "HoleFilling" ${test_dir}/${test}_${dim}x${dim}.dlm v1 1000
  echo ""
done

echo "Analog v2"
for dim in ${dims[@]} ; do
  echo $dim
  ./time-cnn-sim-openmp-simd "HoleFilling" ${test_dir}/${test}_${dim}x${dim}.dlm v2 1000 1
  echo ""
done

echo "Difference Equation v2"
for dim in ${dims[@]} ; do
  echo $dim
  ./time-cnn-sim-openmp-simd "HoleFilling" ${test_dir}/${test}_${dim}x${dim}.dlm v2 1000
  echo ""
done

