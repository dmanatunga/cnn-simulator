#include <string>
#include <iostream>

#include "utilities/BWImage.h"
#include "cnn/cnn.h"
#include "openmp_simd/CNNArrayOpenMPSIMD.h"

#include "timer.hpp"

#define NUM_TRIALS 5
using namespace std;
int main(int argc, char* argv[])
{
  bool dt_given;
  float dt;
  if (argc == 5) {
    dt_given = false;
  } else if (argc == 6) {
    dt_given = true;
    dt = (float)atof(argv[5]);
  } else {
    // Error on necessary parameters
    cerr << "Invalid number of arguments. Expected 4 or 5 arguments. Received " << argc << " arguments." << endl;
    return -1;
  }

  string geneName = string(argv[1]);
  string testInputFile = string(argv[2]);
  string CNNversion = string(argv[3]);
  unsigned int N = (unsigned int)atoi(argv[4]);
  CNNGene* gene = CNNGeneFactory::createCNNGene(geneName);

  BWImage testIn, correctOutput;
  testIn.dlmread(testInputFile, " ");

  CNN* array;
  if (CNNversion == "v1")
    array = new CNNArrayOpenMPSIMD(testIn.R(), testIn.C(), gene->r());
  else if (CNNversion == "v2")
    array = new CNNArrayOpenMPSIMDv2(testIn.R(), testIn.C(), gene->r());
  else {
    // Error on necessary parameters
    cerr << "Invalid CNN version paratmer: " << CNNversion << endl;
    return -1;
  }

  CNNInput* input = gene->getInput(testIn.getImage(), testIn.R(), testIn.C());
  
  double min_time_passed;
  if (dt_given) {
    timer cnn_timer;
    array->run(gene, input, N, dt);
    min_time_passed = cnn_timer.get_ms(); 
  } else {
    timer cnn_timer;
    array->run(gene, input, N);
    min_time_passed = cnn_timer.get_ms(); 
  }
  
  cout << "Time (ms): " << min_time_passed << endl;
  delete gene;
  delete input;
  delete array;

  return 0;
}
