#ifndef CNN_ARRAY_SIMD_H
#define CNN_ARRAY_SIMD_H

#include "cnn/cnn.h"
#include <assert.h>
class CNNArraySIMD : public CNN {
  private:
    float* _state;
    float* _output;

    size_t _R;
    size_t _C;

  public:
    CNNArraySIMD(size_t M, size_t N, size_t r);   
    ~CNNArraySIMD();

    void run(CNNGene* gene, CNNInput *input, unsigned int N, float dt);
    void run(CNNGene* gene, CNNInput *input, unsigned int N);
    void copyOutput(float* out);

  private:
    void computeNextState(float dt);
    void computeNextState();
    void computeInitialOutput();
    void computeOutput();

    
	  unsigned int convergenceRun(CNNGene* gene, CNNInput *input, float dt) {assert(false); }
	  unsigned int convergenceRun(CNNGene* gene, CNNInput *input) { assert(false);}
	  std::vector<long long int> outputAnalysisRun(CNNGene* gene, CNNInput* input, float* idealOutput) { assert(false); }
};

class CNNArraySIMDv2 : public CNN {
  private:
    float* _state;
    float* _output;
    float* _tmp_output;

    size_t _R;
    size_t _C;

  public:
    CNNArraySIMDv2(size_t M, size_t N, size_t r);   
    ~CNNArraySIMDv2();

    void run(CNNGene* gene, CNNInput *input, unsigned int N, float dt);
    void run(CNNGene* gene, CNNInput *input, unsigned int N);
    void copyOutput(float* out);

  private:
    void computeStateAndOutput(float dt);
    void computeStateAndOutput();
    void computeInitialOutput();
    void computeInitialOutput2();
	  
	  unsigned int convergenceRun(CNNGene* gene, CNNInput *input, float dt) {assert(false); }
	  unsigned int convergenceRun(CNNGene* gene, CNNInput *input) { assert(false);}
	  std::vector<long long int> outputAnalysisRun(CNNGene* gene, CNNInput* input, float* idealOutput) { assert(false); }
};
#endif // CNN_ARRAY_SIMD_H
