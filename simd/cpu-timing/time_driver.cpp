#include <string>
#include <iostream>

#include "utilities/BWImage.h"
#include "cnn/cnn.h"
#include "simd/CNNArraySIMD.h"

#include "timer.hpp"

#define NUM_TRIALS 10
using namespace std;
int main(int argc, char* argv[])
{
  if (argc != 4) {
    // Error on necessary parameters
    cerr << "Invalid number of arguments. Expected 4 or 5 arguments. Received " << argc << " arguments." << endl;
    return -1;
  }

  string geneName = string(argv[1]);
  string testInputFile = string(argv[2]);
  unsigned int N = (unsigned int)atoi(argv[3]);
  CNNGene* gene = CNNGeneFactory::createCNNGene(geneName);

  BWImage testIn, correctOutput;
  testIn.dlmread(testInputFile, " ");

  CNNArraySIMDv2 cnn(testIn.R(), testIn.C(), gene->r());
  CNNInput* input = gene->getInput(testIn.getImage(), testIn.R(), testIn.C());
  
  for (int i = 1; i < NUM_TRIALS; i++) {
    timer cnn_timer;
    cnn.run(gene, input, N);
    double min_time_passed = cnn_timer.get_ms(); 
    cout << "Total Time (ms): " << min_time_passed << endl;
  }
  delete gene;
  delete input;

  return 0;
}
