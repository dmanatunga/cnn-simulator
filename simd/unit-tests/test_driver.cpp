#include <string>
#include <iostream>
#include "utilities/BWImage.h"
#include "cnn/cnn.h"
#include "simd/CNNArraySIMD.h"

using namespace std;
void testSIMD(CNNGene* gene, CNNInput* input, string testInputFile, BWImage* testIn, BWImage* correctOutput, unsigned int N, float dt)
{
  cout << "Creating SIMD-CNN Array of size " << testIn->R() << "x" << testIn->C() << "..." << endl; 
  CNNArraySIMD array(testIn->R(), testIn->C(), gene->r());

  cout << "Running Analog CNN for " << N << " iterations (dt=" << dt << ")..." << endl; 
  array.run(gene, input, N, dt);
  cout << "CNN Simulation complete." << endl;
  BWImage analogOut(testIn->R(), testIn->C());
  array.copyOutput(analogOut.getImage());

  bool pass1 = correctOutput->compare(&analogOut);
  if (pass1) {
    cout << "Analog Test Passed!" << endl;
  } else {
    cout << "Analog Test Failed..." << endl;
    analogOut.dlmwrite(testInputFile + "_analogIncorrect", " ");
  }

  cout << "Running Difference Equation CNN for " << N << " iterations.." << endl; 
  array.run(gene, input, N);
  cout << "CNN Simulation complete." << endl;
  BWImage diffOut(testIn->R(), testIn->C());
  array.copyOutput(diffOut.getImage());
  
  cout << "Comparing CNN output with correct output..." << endl;

  bool pass2 = correctOutput->compare(&diffOut);
  if (pass2) {
    cout << "Difference Test Passed!" << endl;
  } else {
    cout << "Difference Test Failed..." << endl;
    diffOut.dlmwrite(testInputFile + "_diffIncorrect", " ");
  }
}

void testSIMDv2(CNNGene* gene, CNNInput* input, string testInputFile, BWImage* testIn, BWImage* correctOutput, unsigned int N, float dt)
{
  cout << "Creating SIMD-CNNv2 Array of size " << testIn->R() << "x" << testIn->C() << "..." << endl; 
  CNNArraySIMDv2 array(testIn->R(), testIn->C(), gene->r());

  cout << "Running Analog CNN for " << N << " iterations (dt=" << dt << ")..." << endl; 
  array.run(gene, input, N, dt);
  cout << "CNN Simulation complete." << endl;
  BWImage analogOut(testIn->R(), testIn->C());
  array.copyOutput(analogOut.getImage());

  cout << "Running Difference Equation CNN for " << N << " iterations.." << endl; 
  array.run(gene, input, N);
  cout << "CNN Simulation complete." << endl;
  BWImage diffOut(testIn->R(), testIn->C());
  array.copyOutput(diffOut.getImage());
  
  cout << "Comparing CNN output with correct output..." << endl;
  bool pass1 = correctOutput->compare(&analogOut);
  if (pass1) {
    cout << "Analog Test Passed!" << endl;
  } else {
    cout << "Analog Test Failed..." << endl;
    analogOut.dlmwrite(testInputFile + "_v2analogIncorrect", " ");
  }

  bool pass2 = correctOutput->compare(&diffOut);
  if (pass2) {
    cout << "Difference Test Passed!" << endl;
  } else {
    cout << "Difference Test Failed..." << endl;
    diffOut.dlmwrite(testInputFile + "_v2diffIncorrect", " ");
  }
}


int main(int argc, char* argv[])
{
  int num_args_needed =6;
  if (argc != num_args_needed)
  {
    // Error on necessary parameters
    cerr << "Invalid number of arguments. Expected " << num_args_needed << " arguments. Received " << argc << " arguments." << endl;
    return -1;
  }

  string geneName = string(argv[1]);
  string testInputFile = string(argv[2]);
  string correctOutFile = string(argv[3]);
  unsigned int N = (unsigned int)atoi(argv[4]);
  float dt = (float)atof(argv[5]);

  cout << "Creating CNN Gene " << geneName << "..." << endl; 
  CNNGene* gene = CNNGeneFactory::createCNNGene(geneName);

  cout << "Reading test input..." << endl;
  BWImage testIn, correctOutput;
  testIn.dlmread(testInputFile, " ");
  correctOutput.dlmread(correctOutFile, " ");
  CNNInput* input = gene->getInput(testIn.getImage(), testIn.R(), testIn.C());
  testSIMD(gene, input, testInputFile, &testIn, &correctOutput, N, dt);
  cout << endl;
  testSIMDv2(gene, input, testInputFile, &testIn, &correctOutput, N, dt);
  delete gene;
  delete input;

  return 0;
}
