#! /usr/bin/python
import os
import shutil
import sys

RESULTS_DIR = '/user/dmanatunga/cnn/results'
SIM_DIR = '/user/dmanatunga/cnn/cnn-simulator'
SIM_TEST_DIR = '/user/dmanatunga/cnn/cnn-simulator/test_files'
SIM_CORRECT_OUTPUT_DIR = '/user/dmanatunga/cnn/cnn-simulator/test_outputs'

RUN_FILE = 'run.py'
RUN_TEST_DIR = 'test_files'
RUN_CORRECT_OUTPUT_DIR = 'correct_output'

NAIVE_SIM_DIR = '/user/dmanatunga/cnn/cnn-simulator/sp_cnn/naive-sp_cnn'
NAIVE_BIN = 'naive-sp-cnn'

SP_CNN_SIM_DIR = '/user/dmanatunga/cnn/cnn-simulator/sp_cnn/sp_cnn'
SP_CNN_BIN = 'sp-cnn'

#GENES = ['ConcentricContour', 'ConnectedComponent', 'CornerDetection', 'EdgeDetection', 'HoleFilling', 'RotationDetector', 'ShadowCreator']
GENES = ['ConnectedComponent', 'CornerDetection', 'EdgeDetection', 'HoleFilling', 'RotationDetector', 'ShadowCreator']
TESTS = ['lowerA', 'capitalA', 'vertLine', 'circle', 'rect', 'filledSquare', 'zero', 'seven', 'eight', 'nine']
DIMS = [1024, 2048]
INTERVALS = [16, 32, 64, 128, 192, 256]
CNN_DIM = [128, 128]
GLOBAL_NUM_CNN_UNITS = [1, 2, 4, 8]
RESULTS_FILENAME = 'results.out'

def run_sp_cnn(run_name, run_param, run_type, gene, dim, numUnits, interval, bench_base_dir, run_dir):
    file_name = os.path.join(bench_base_dir, RUN_FILE)
    with open(file_name, 'w') as file:
        file.write('#!/usr/bin/python\n\n')
        file.write('import os\n')
        file.write('import glob\n')
        file.write('import sys\n\n')
        file.write('ppid = os.getppid()\n')
        bench = '%s_%d' % (gene, dim)
        file.write('test_dir = \'/tmp/cnnsim_\' + \'%s_\' + str(ppid) + \'/%s\'\n' % (run_name, bench))
        file.write('os.chdir(\'%s\')\n' % (bench_base_dir))
        file.write('os.system(\'uname -a\')\n')
        file.write('os.system(\'mkdir -p \%s\' % (test_dir))\n')
        file.write('os.system(\'cp -r %s/%s %%s/%s\' %% (test_dir))\n' % (run_dir, RUN_TEST_DIR, RUN_TEST_DIR))
        file.write('os.system(\'cp -r %s/%s %%s/%s\' %% (test_dir))\n' % (run_dir, RUN_CORRECT_OUTPUT_DIR, RUN_CORRECT_OUTPUT_DIR))
        file.write('os.system(\'cp %s/%s %%s\' %% (test_dir))\n' % (run_dir, SP_CNN_BIN))
        file.write('os.chdir(\'%s\' % (test_dir))\n')
       
        file.write('print(\'Running Tests...\')\n')
        for test in TESTS:
            test_name = '%s/%s_%dx%d.dlm' % (RUN_TEST_DIR,test, dim, dim)
            correct_output_name = '%s/%s_%s_%dx%d.dlm' % (RUN_CORRECT_OUTPUT_DIR, gene, test, dim, dim)
            file.write('os.system(\'./%s %s %d %d %d %d %s %%s/%s %s %%s/%s %s >> %s\' %% (test_dir, test_dir))\n' %
                                    (SP_CNN_BIN,
                                     run_type,
                                     CNN_DIM[0], CNN_DIM[1],
                                     numUnits, interval,
                                     gene, test_name, 'row-major', 
                                     correct_output_name, run_param, RESULTS_FILENAME))
        file.write('print(\'Tests Completed...\')\n')
        file.write('os.system(\'mv %s %s/%s\')\n' % (RESULTS_FILENAME, bench_base_dir, RESULTS_FILENAME))
        file.write('os.system(\'rm -rf %s\' % (test_dir))\n')
        file.close()

    os.system('chmod +x %s' % (file_name))
   
    # qsub command
    cmd = []
    cmd += ['qsub']
    cmd += [RUN_FILE]
    cmd += ['-V -m n']
    cmd += ['-o', '%s/qsub.stdout' % (bench_base_dir)]
    cmd += ['-e', '%s/qsub.stderr' % (bench_base_dir)]
    cmd += ['-q', 'pool1']
    cmd += ['-N', '%s_%s' % (run_name, bench)]
    cmd += ['-l', 'nodes=1:ppn=1']

    cwd = os.getcwd()
    os.chdir('%s' % (bench_base_dir))
    os.system('/bin/echo \'%s\' > %s/TORQUE_RUN_CMD' % (' '.join(cmd), bench_base_dir))
    os.system('chmod +x %s/TORQUE_RUN_CMD' % bench_base_dir)
    os.system('%s | tee %s/JOB_ID' % (' '.join(cmd), bench_base_dir))
    os.chdir(cwd)

def run_naive(run_name, run_param, gene, dim, bench_base_dir, run_dir):
    file_name = os.path.join(bench_base_dir, RUN_FILE)
    with open(file_name, 'w') as file:
        file.write('#!/usr/bin/python\n\n')
        file.write('import os\n')
        file.write('import glob\n')
        file.write('import sys\n\n')
        file.write('ppid = os.getppid()\n')
        bench = '%s_%d' % (gene, dim)
        file.write('test_dir = \'/tmp/cnnsim_\' + \'%s_\' + str(ppid) + \'/%s\'\n' % (run_name, bench))
        file.write('os.chdir(\'%s\')\n' % (bench_base_dir))
        file.write('os.system(\'uname -a\')\n')
        file.write('os.system(\'mkdir -p \%s\' % (test_dir))\n')
        file.write('os.system(\'cp -r %s/%s %%s/%s\' %% (test_dir))\n' % (run_dir, RUN_TEST_DIR, RUN_TEST_DIR))
        file.write('os.system(\'cp -r %s/%s %%s/%s\' %% (test_dir))\n' % (run_dir, RUN_CORRECT_OUTPUT_DIR, RUN_CORRECT_OUTPUT_DIR))
        file.write('os.system(\'cp %s/%s %%s\' %% (test_dir))\n' % (run_dir, NAIVE_BIN))
        file.write('os.chdir(\'%s\' % (test_dir))\n')
       
        file.write('print(\'Running Tests...\')\n')
        for test in TESTS:
            test_name = '%s/%s_%dx%d.dlm' % (RUN_TEST_DIR,test, dim, dim)
            correct_output_name = '%s/%s_%s_%dx%d.dlm' % (RUN_CORRECT_OUTPUT_DIR, gene, test, dim, dim)
            file.write('os.system(\'./%s %d %d %s %%s/%s %%s/%s %s >> %s\' %% (test_dir, test_dir))\n' % 
                                    (NAIVE_BIN,
                                     CNN_DIM[0], CNN_DIM[1],
                                     gene, test_name, correct_output_name, run_param, RESULTS_FILENAME))
        file.write('print(\'Tests Completed...\')\n')
        file.write('os.system(\'mv %s %s/%s\')\n' % (RESULTS_FILENAME, bench_base_dir, RESULTS_FILENAME))
        file.write('os.system(\'rm -rf %s\' % (test_dir))\n')
        file.close()

    os.system('chmod +x %s' % (file_name))
   
    # qsub command
    cmd = []
    cmd += ['qsub']
    cmd += ['run.py']
    cmd += ['-V -m n']
    cmd += ['-o', '%s/qsub.stdout' % (bench_base_dir)]
    cmd += ['-e', '%s/qsub.stderr' % (bench_base_dir)]
    cmd += ['-q', 'pool1']
    cmd += ['-N', '%s_%s' % (run_name, bench)]
    cmd += ['-l', 'nodes=1:ppn=1']

    cwd = os.getcwd()
    os.chdir('%s' % (bench_base_dir))
    os.system('/bin/echo \'%s\' > %s/TORQUE_RUN_CMD' % (' '.join(cmd), bench_base_dir))
    os.system('chmod +x %s/TORQUE_RUN_CMD' % bench_base_dir)
    os.system('%s | tee %s/JOB_ID' % (' '.join(cmd), bench_base_dir))
    os.chdir(cwd)


def cnnsim(run_name, run_type, run_param):
    cur_dir = os.getcwd()
    run_dir = os.path.join(RESULTS_DIR, run_name)

    if os.path.exists(run_dir):
        print("Erasing current directory...");
        shutil.rmtree(run_dir)
      
    os.makedirs(run_dir)
    os.makedirs(os.path.join(run_dir, RUN_TEST_DIR))
    os.makedirs(os.path.join(run_dir, RUN_CORRECT_OUTPUT_DIR))

    print('Copying test and output files...')
    for test in TESTS:
        for dim in DIMS:
            test_name = '%s_%dx%d.dlm' % (test, dim, dim)
            shutil.copyfile(os.path.join(SIM_TEST_DIR, test_name), os.path.join(run_dir, RUN_TEST_DIR, test_name))

            for gene in GENES:
                correct_output_name = '%s_%s' % (gene, test_name)
                shutil.copyfile(os.path.join(SIM_CORRECT_OUTPUT_DIR, gene, correct_output_name),
                                os.path.join(run_dir, RUN_CORRECT_OUTPUT_DIR, correct_output_name))
    if run_type == 'naive':
        shutil.copyfile(os.path.join(NAIVE_SIM_DIR, NAIVE_BIN), os.path.join(run_dir, NAIVE_BIN))
        shutil.copystat(os.path.join(NAIVE_SIM_DIR, NAIVE_BIN), os.path.join(run_dir, NAIVE_BIN))

        print('Generating TORQUE test files...')
        for gene in GENES:
            for dim in DIMS:
                gene_dir = os.path.join(run_dir, '%s_%d' % (gene, dim))
                os.mkdir(gene_dir)
                run_naive(run_name, run_param, gene, dim, gene_dir, run_dir)
    elif run_type == 'fixed-interval' or run_type == 'early-finish':
        shutil.copyfile(os.path.join(SP_CNN_SIM_DIR, SP_CNN_BIN), os.path.join(run_dir, SP_CNN_BIN))
        shutil.copystat(os.path.join(SP_CNN_SIM_DIR, SP_CNN_BIN), os.path.join(run_dir, SP_CNN_BIN))

        if run_param == 'fast':
            NUM_CNN_UNITS = [1]    
        else:
            NUM_CNN_UNITS = GLOBAL_NUM_CNN_UNITS


        print('Generating TORQUE test files...')
        for gene in GENES:
            for dim in DIMS:
                for numUnits in NUM_CNN_UNITS:
                    for interval in INTERVALS:
                        bench_dir = os.path.join(run_dir, '%s_%d_%d_%d' % (gene, dim, numUnits, interval))
                        os.mkdir(bench_dir)
                        run_sp_cnn(run_name, run_param, run_type, gene, dim, numUnits, interval, bench_dir, run_dir)
    else:
        print('Invalid Run Type')

if __name__ == "__main__":
    if len(sys.argv) == 4:
        run_name = sys.argv[1]
        run_type = sys.argv[2]
        run_param = sys.argv[3]
        cnnsim(run_name, run_type, run_param)
    else:
        print('Usage %s <run-name> <run-type> <run-param>' % sys.argv[0])
